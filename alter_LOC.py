# alternative version of LOC

import utils as utilities
import networkx as nx
from bs4 import NavigableString
from fuzzywuzzy import fuzz
from event_handler import EHD


class LOC:
    # Inits
    def __init__(self, current_queue, previous_queue, matched_queue, content_detection, presentation_detection, behaviour_detection):
        self.current_queue: list = current_queue
        self.previous_queue: list = previous_queue
        self.matched_queue: list = matched_queue
        self.content_detection: list = content_detection

        # New queues
        self.presentation_detection: list = presentation_detection
        self.behaviour_detection: list = behaviour_detection

        # Initializing EH parser
        self.event_handler_parser = EHD()

    # Function that looks for element in all possible queue
    def findElementInQueues(self, element_uid, mode):

        if isinstance(element_uid, int):
            if mode == 0:

                # Current queue
                for element in self.current_queue:
                    if element_uid == element[0]:
                        # print("[LOC - FEIQ] Element found in current queue!")
                        return [0, element]

                # Matched queue
                for pair in self.matched_queue:
                    # Selecting appr. element in pair
                    current_element_from_pair = pair[mode]

                    if element_uid == current_element_from_pair[0]:
                        # print( "[LOC - FEIQ] Element found in matched queue!" )
                        return [2, pair]

                # After parsing all queues and elements was not found
                return False

            elif mode == 1:

                # Previous queue
                for element in self.previous_queue:
                    if element_uid == element[0]:
                        # print("[LOC - FEIQ] Element found in previous queue!")
                        return [0, element]

                # Matched queue
                for pair in self.matched_queue:
                    # Selecting appr. element in pair
                    current_element_from_pair = pair[mode]

                    if element_uid == current_element_from_pair[0]:
                        # print( "[LOC - FEIQ] Element found in matched queue!" )
                        return [2, pair]

                # After parsing all queues and elements was not found
                return False

            else:
                print("[LOC - FEIQ] Wrong mode selection!")
        else:
            print("[LOC - FEIQ] ID is not int-type!")

    # LOC processing function
    def LOC_processing(self, current_element, previous_element):
        # Extracting the data
        current_parent_uid = current_element[1]
        previous_parent_uid = previous_element[1]

        current_parent_element = self.findElementInQueues(current_parent_uid, 0)
        previous_parent_element = self.findElementInQueues(previous_parent_uid, 1)

        # LOC logic
        if isinstance(current_parent_element, list) and isinstance(previous_parent_element, list):

            # Checking for indicators, based on indicator ---> build logic processing
            current_element_indicator = current_parent_element[0]
            previous_element_indicator = previous_parent_element[0]

            # If both belong element to matched queue
            if current_element_indicator == 2 and previous_element_indicator == 2:

                # Extracting pair
                current_element = current_parent_element[1][0]
                previous_element = previous_parent_element[1][1]

                # Check if parent elements are the same pair
                if current_parent_element == previous_parent_element:
                    print("[LOC] Found parent pair has same indicators (%s, %s) and parent elements are the same!" % (current_element_indicator, previous_element_indicator))
                    print("\n")

                    # Return pairs for recursion
                    return [current_element, previous_element]
                else:
                    print("[LOC - Error] Found parent pair has same indicators (%s, %s) and parent elements are different!" % (current_element_indicator, previous_element_indicator))
                    print("\n")

            # If both belong element to regular queue
            elif current_element_indicator == 0 and previous_element_indicator == 0:

                print("[LOC] Regular elements in processing...")

                # Extracting data
                current_element = current_parent_element[1]
                previous_element = previous_parent_element[1]

                # Extracting node for decision
                current_node = utilities.extractdatafromgraph(current_element[0], current_element[3])
                previous_node = utilities.extractdatafromgraph(previous_element[0], previous_element[3])

                candidate_for_matched_flag = False

                # Checking label and weight
                if current_node["tag"] == previous_node["tag"] and current_element[2] >= previous_element[2]:
                    # If tags + attrs + contents are identical -> means that both elements are completely identical

                    # Checking attributes
                    if current_node["attrs"] != previous_node["attrs"]:
                        # Local inits
                        current_attr_dictionary: dict = current_node["attrs"]
                        previous_attr_dictionary: dict = previous_node["attrs"]

                        # Checking for behav. attributes
                        current_eh_list = self.event_handler_parser.detectEventHandlers(current_attr_dictionary)
                        previous_eh_list = self.event_handler_parser.detectEventHandlers(previous_attr_dictionary)

                        # Removing event-handlers from original attributes dictionaries
                        # Might be run-time issue
                        if len(current_eh_list) > 0:
                            for key, value in current_attr_dictionary.copy().items():
                                if key in current_eh_list:
                                    current_attr_dictionary.pop(key)

                        if len(previous_eh_list) > 0:
                            for key, value in previous_attr_dictionary.copy().items():
                                if key in previous_eh_list:
                                    previous_attr_dictionary.pop(key)

                        # Comparing both lists
                        EH_comparison = utilities.inspectAttributesForSimilarity(current_eh_list, previous_eh_list)
                        ATTR_comparison = utilities.inspectAttributesForSimilarity(current_attr_dictionary, previous_attr_dictionary)

                        print("Current EH list: ", current_eh_list)
                        print("Previous EH list: ", previous_eh_list)
                        print("EH comparison: ", EH_comparison)

                        print("Current list wo EH: ", current_attr_dictionary)
                        print("Previous list wo EH: ", previous_attr_dictionary)
                        print("ATTR comparison: ", ATTR_comparison)

                        if ATTR_comparison is False:
                            self.presentation_detection.append([current_element, previous_element])

                        if EH_comparison is False:
                            status = "modification of event handlers"
                            self.behaviour_detection.append([current_element, previous_element, status])

                    if current_node["SHA_content"] == previous_node["SHA_content"]:
                        print("[LOC] Regular elements are candidates for matching!")
                        candidate_for_matched_flag = True
                    else:
                        # Content detection phase - at this stage we know that node belongs to structure (structure propagation)
                        print( "[LOC] Content changed SHAs (%s, %s)" % (current_node["SHA_content"], previous_node["SHA_content"]) )
                        # Content detection scheme
                        # Extract text from nodes
                        current_dom = current_node["DOM"]
                        previous_dom = previous_node["DOM"]

                        current_dom_content = [element for element in current_dom if isinstance( element, NavigableString )]
                        previous_dom_content = [element for element in previous_dom if isinstance( element, NavigableString )]

                        percent = fuzz.ratio( current_dom_content, previous_dom_content )

                        if percent >= 40:
                            print( "Text has updated! TSR: %i" % percent )
                            print( "Current content: %s" % current_dom_content )
                            print( "Previous content: %s" % previous_dom_content )
                            status = "Updated"
                        else:
                            print( "Text has modified! TSR: %i" % percent )
                            print( "Current content: %s" % current_dom_content )
                            print( "Previous content: %s" % previous_dom_content )
                            status = "Modified"

                        self.content_detection.append([current_element, previous_element, percent, status])

                    #---------------------------------------------------------------

                    # Removal and appending to appr. queues
                    self.current_queue.remove(current_element)
                    self.previous_queue.remove(previous_element)
                    self.matched_queue.append([current_element, previous_element])

                    # Return pairs for recursion
                    return [current_element, previous_element]
                    print("\n")

                else:
                    print("[LOC - exit_condition] Element tags and attributes are not the same! Cannot match the elements...\n")
            else:
                print("[LOC - Error] Indicators for elements are not the same (%s -- %s). Elements (%s -- %s) are not related...\n" % (current_element_indicator, previous_element_indicator, current_parent_element[1], previous_parent_element[1]))
        else:
            print("[LOC - Error] Elements search failed for some element! Elements: [%s -- %s] \n" % (current_parent_element, previous_parent_element))

    def analyse(self, start_index, end_index):
        # LOC
        print("[LOC] Phase initialized...")
        # print("Start/End: %i, %i" % (i, current_inv_elements))

        start = start_index
        end = end_index

        # LOC procedure
        for start in range(start, end):
            # Extracting node elements from matching array
            element_pair = self.matched_queue[start]

            current_matched_element = element_pair[0]
            previous_matched_element = element_pair[1]

            # Looking for nodes that has a parent for the specific node
            print("[LOC] Matched queue pair elements: %s, %s" % (current_matched_element, previous_matched_element))

            result = self.LOC_processing(current_matched_element, previous_matched_element)

            # Structure propagation
            iteration_count = 0

            # If function runs list -> structure propagation
            while isinstance(result, list):
                print("[LOC] Structure propagation, iteration number: ", iteration_count)
                result = self.LOC_processing(result[0], result[1])
                iteration_count += 1
