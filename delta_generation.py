# Phase 3 - Post-processing
# Class that is responsible for delta file generation

from lxml import etree
import datetime
import uuid


class DeltaGen:

    def __init__(self, composite_queue, current_path, previous_path, total_current, total_previous):
        self.matched_queue: list = composite_queue[0]

        # Type of changes
        self.structure_detection: list = composite_queue[1]
        self.content_detection: list = composite_queue[2]
        self.presentation_detection: list = composite_queue[3]
        self.behaviour_detection: list = composite_queue[4]

        self.current_path = current_path
        self.previous_path = previous_path

        # Element amount in queue
        self.total_current = total_current
        self.total_previous = total_previous

        # [self.matched_queue, self.structure_detection, self.content_detection, self.presentation_detection, , self.behaviour_detection]

    # Function parses the structural queue and counts different types of structural operations
    def countStructuralDifferences(self):

        moved_counter = 0
        inserted_counter = 0
        removed_counter = 0

        for element in self.structure_detection:

            status: str = element[2]

            if status == "moved":
                moved_counter += 1
            elif status == "inserted":
                inserted_counter += 1
            elif status == "removed":
                removed_counter += 1

        return [moved_counter, inserted_counter, removed_counter]

    # Functions speak for themselves
    def clusterStructuralChanges(self):

        counters = self.countStructuralDifferences()

        local_root = etree.Element("structural_changes", overall=str(len(self.structure_detection)))
        local_root.set("moved", str(counters[0]))
        local_root.set("new", str(counters[1]))
        local_root.set("removed", str(counters[2]))

        for element in self.structure_detection:
            delta_object = etree.Element("structure_change")

            # Extracting status
            object_status = element[2]
            delta_status = etree.Element("status")
            delta_status.text = object_status
            delta_object.append(delta_status)

            # Checking contents
            if element[0] is not None:
                delta_first_object = element[0]
                delta_node = delta_first_object[3].nodes[delta_first_object[0]]
                delta_dom = delta_node["DOM"]
                document_dom = etree.Element("current_DOM")
                document_dom.text = str(delta_dom)
                delta_object.append(document_dom)

            if element[1] is not None:
                delta_second_object = element[1]
                delta_node = delta_second_object[3].nodes[delta_second_object[0]]
                delta_dom = delta_node["DOM"]
                document_dom = etree.Element("previous_DOM")
                document_dom.text = str(delta_dom)
                delta_object.append(document_dom)

            local_root.append(delta_object)

        return local_root

    def clusterPresentationChanges(self):
        local_root = etree.Element("presentation_changes", count=str(len(self.presentation_detection)))

        for element in self.presentation_detection:
            delta_object = etree.Element("presentation_change")

            # Extracting data
            current_element = element[0]
            previous_element = element[1]

            current_node = current_element[3].nodes[current_element[0]]
            previous_node = previous_element[3].nodes[previous_element[0]]

            current_attrs = current_node["attrs"]
            previous_attrs = previous_node["attrs"]

            # Delta presentations
            current_delta_attrs = etree.Element("current_attributes")
            current_delta_attrs.text = str(current_attrs)

            previous_delta_attrs = etree.Element("previous_attributes")
            previous_delta_attrs.text = str(previous_attrs)

            current_dom = current_node["DOM"]
            previous_dom = previous_node["DOM"]

            current_delta_dom = etree.Element("current_DOM")
            current_delta_dom.text = str(current_dom)

            previous_delta_dom = etree.Element("previous_DOM")
            previous_delta_dom.text = str(previous_dom)

            delta_object.append(current_delta_attrs)
            delta_object.append(previous_delta_attrs)
            delta_object.append(current_delta_dom)
            delta_object.append(previous_delta_dom)

            local_root.append(delta_object)

        return local_root

    def clusterContentChanges(self):
        local_root = etree.Element("content_changes", count=str(len(self.content_detection)))

        # [current_element, previous_element, percent, status]

        for element in self.content_detection:
            delta_object = etree.Element("content_change")

            # Extracting the data
            current_element = element[0]
            previous_element = element[1]
            percent = element[2]
            status = element[3]

            delta_percent = etree.Element("percent")
            delta_percent.text = str(percent)

            delta_status = etree.Element("status")
            delta_status.text = str(status)

            # Content object
            delta_current_content = etree.Element("current_content")
            delta_previous_content = etree.Element("previous_content")

            current_node = current_element[3].nodes[current_element[0]]
            previous_node = previous_element[3].nodes[previous_element[0]]

            current_content = current_node["tag_content"]
            previous_content = previous_node["tag_content"]

            delta_current_content.text = str(current_content)
            delta_previous_content.text = str(previous_content)

            current_dom = current_node["DOM"]
            previous_dom = previous_node["DOM"]

            current_delta_dom = etree.Element("current_DOM")
            current_delta_dom.text = str(current_dom)

            previous_delta_dom = etree.Element("previous_DOM")
            previous_delta_dom.text = str(previous_dom)

            delta_object.append(delta_percent)
            delta_object.append(delta_status)
            delta_object.append(delta_current_content)
            delta_object.append(delta_previous_content)
            delta_object.append(current_delta_dom)
            delta_object.append(previous_delta_dom)

            local_root.append(delta_object)

        return local_root

    def clusterBehaviouralChanges(self):
        local_root = etree.Element("behavioural_changes", count=str(len(self.behaviour_detection)))

        for element in self.behaviour_detection:

            delta_object = etree.Element("behavioural_change")

            # diff.append([each, status])
            # diff.append([current_element, previous_element, status])

            if len(element) == 3:
                # Extracting information
                current_element = element[0]
                previous_element = element[1]
                status = element[2]

                delta_status = etree.Element("status")
                delta_status.text = str(status)

                delta_current_dom = etree.Element("current_DOM")
                current_element_node = current_element[3].nodes[current_element[0]]
                current_dom = current_element_node["DOM"]
                delta_current_dom.text = str(current_dom)

                delta_previous_dom = etree.Element("previous_DOM")
                previous_element_node = previous_element[3].nodes[previous_element[0]]
                previous_dom = previous_element_node["DOM"]
                delta_previous_dom.text = str(previous_dom)

                # Appending blocks to general composition
                delta_object.append(delta_status)
                delta_object.append(delta_current_dom)
                delta_object.append(delta_previous_dom)

                local_root.append(delta_object)

            elif len(element) == 2:
                # Extracting information
                selected_element = element[0]
                status = element[1]

                selected_element_node = selected_element[3].nodes[selected_element[0]]
                selected_dom = selected_element_node["DOM"]

                delta_status = etree.Element("status")
                delta_status.text = str(status)

                delta_dom = etree.Element("dom")
                delta_dom.text = str(selected_dom)

                # Appending blocks to general composition
                delta_object.append(delta_status)
                delta_object.append(delta_dom)
                local_root.append(delta_object)

        return local_root

    def generateReport(self):

        root: etree = etree.Element("report")

        # General information block
        general_info = etree.Element("general_information")

        # Timestamp
        generation_time = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        timestamp = etree.Element("timestamp")
        timestamp.text = generation_time

        # Total nodes for each code iteration
        total_elements = etree.Element("total_elements")

        sum_current_elements = etree.Element("current_elements")
        sum_current_elements.text = str(self.total_current)

        sum_previous_elements = etree.Element("previous_elements")
        sum_previous_elements.text = str(self.total_previous)

        total_elements.append(sum_current_elements)
        total_elements.append(sum_previous_elements)

        # Paths
        paths = etree.Element("paths")

        current_path = etree.Element("current_file_path")
        current_path.text = self.current_path

        previous_path = etree.Element("previous_file_path")
        previous_path.text = self.previous_path

        paths.append(current_path)
        paths.append(previous_path)

        # Clustering the general block
        general_info.append(timestamp)
        general_info.append(total_elements)
        general_info.append(paths)

        # Clustering detection blocks
        detection_info = etree.Element("detection_summary")
        detection_info.append(self.clusterStructuralChanges())
        detection_info.append(self.clusterPresentationChanges())
        detection_info.append(self.clusterContentChanges())
        detection_info.append(self.clusterBehaviouralChanges())

        # Clustering root-elements
        root.append(general_info)
        root.append(detection_info)

        overall_content = etree.tostring(root, pretty_print=True)

        with open("report.xml", "wb") as delta_file:
            delta_file.write(overall_content)

