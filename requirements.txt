matplotlib==3.2.1
lxml==4.5.0
fuzzywuzzy==0.18.0
networkx==2.4
beautifulsoup4==4.8.2
