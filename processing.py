# Phase 2 - Processing
# Class that is responsible for sequential execution of processing sub-classes

from WDA import WDA
from LOC import LOC
# from alter_LOC import LOC as aLOC
from alter_BIA import BIA as aBIA
# from BIA import BIA
from BPP import BPP
import utils as utilities


class Processing:

    def __init__(self, queues):
        # Inits
        self.current_queue: list = queues[0]  # For queue elements of current iteration of the code
        self.previous_queue: list = queues[1]  # For queue elements of previous iteration of the code
        self.matched_queue: list = []  # Matched pairs (whole object matching)

        # Separation of output / later will be used for delta
        self.structure_detection: list = []
        self.content_detection: list = []
        self.presentation_detection: list = []
        self.behaviour_detection: list = []

    # Displays meta data of queues -> useful for inspection after sub-process execution
    def showQueueMeta(self):
        print("\nLength of queues [curr/prev/matching] = [%i, %i, %i]" % (len(self.current_queue), len(self.previous_queue), len(self.matched_queue)))
        # print("Matched queue [%s] ", len(self.matched_queue))
        print("Current queue [%s]" % len(self.current_queue))
        print("Previous queue [%s] " % len(self.previous_queue))
        print("Deltas: ")
        print("Structure changes [%s] : %s" % (len(self.structure_detection), self.structure_detection))
        print("Content changes [%s] : %s" % (len(self.content_detection), self.content_detection))
        print("Presentation changes [%s] : %s" % (len(self.presentation_detection), self.presentation_detection))
        print("Behaviour changes [%s] : %s \n" % (len(self.behaviour_detection), self.behaviour_detection))

    # Document - node analysis
    def document_root_analysis(self):
        # Looking for root-node
        current_root_element = utilities.searchinqueue(0, self.current_queue, False, False)
        previous_element_previous = utilities.searchinqueue(0, self.previous_queue, False, False)

        # Removing from queues
        self.current_queue.remove(current_root_element)
        self.previous_queue.remove(previous_element_previous)
        self.matched_queue.append([current_root_element, previous_element_previous])

        # Checking root-element
        if isinstance(current_root_element, list) and isinstance( previous_element_previous, list):
            if utilities.comparedocumentnodes( current_root_element, previous_element_previous) is True:
                print("[Document] Both trees are identical. No need to inspect...")
                return True
            else:
                print("[Document] Trees are not identical. Need to inspect...")
                return False
        else:
            print("[Document] Document nodes were not found in queues! Exiting...")
            exit(-3)

    # Main call
    def execute(self):

        print("Overall information before processing")
        self.showQueueMeta()

        if (isinstance(self.current_queue, list) and isinstance(self.previous_queue, list)) and (self.current_queue is not None and self.previous_queue is not None):

            documents_identical = self.document_root_analysis()

            # Checking the document-node for similarity (it is responsible for whole document)
            if documents_identical is True:
                print("[DIFF-MAIN :: Processing] Both trees are identical. No need to inspect..")
                exit(-2)

            # Phase 2 - #1 BPP
            result = BPP(self.current_queue, self.previous_queue, self.behaviour_detection).analyse()

            # Phase 2 - #2 WDA
            WDA(self.current_queue, self.previous_queue, self.matched_queue).analyse()
            self.matched_queue = utilities.removeDuplicates(self.matched_queue, False)
            self.showQueueMeta()

            # exit(-1)

            # Phase 2 - #3 LOC
            if self.matched_queue is not None and len(self.matched_queue) > 0:
                # Inits
                # Starting from one - since 0 -> is document-element
                start = 1
                end = len(self.matched_queue)

                # Processing only new added elements
                while start != end:
                    LOC(self.current_queue, self.previous_queue, self.matched_queue, self.content_detection).analyse(start, end)
                    start = end
                    end = len(self.matched_queue)

                # Removing duplicates
                # print("[LOC Duplicates] After LOC - Matched ")
                # self.matched_queue = utilities.removeDuplicates(self.matched_queue, True)
                self.showQueueMeta()

            else:
                print("[DIFF-MAIN :: Processing] After WDA, there is no matched elements for LOC analysis. Exiting...")
                exit(-2)

            # Phase 3 - #3.1 alternative LOC
            # if self.matched_queue is not None and len(self.matched_queue) > 0:
            #     # Inits
            #     # Starting from one - since 0 -> is document-element
            #     start = 1
            #     end = len(self.matched_queue)
            #
            #     # Processing only new added elements
            #     while start != end:
            #         aLOC(self.current_queue, self.previous_queue, self.matched_queue, self.content_detection, self.presentation_detection, self.behaviour_detection).analyse(start, end)
            #         start = end
            #         end = len(self.matched_queue)
            #
            #     # Removing duplicates
            #     # print("[LOC Duplicates] After LOC - Matched ")
            #     # self.matched_queue = utilities.removeDuplicates(self.matched_queue, True)
            #     self.showQueueMeta()
            #
            # else:
            #     print("[DIFF-MAIN :: Processing] After WDA, there is no matched elements for LOC analysis. Exiting...")
            #     exit(-2)

            # Phase 2 - #4 BIA
            # if len(self.current_queue) > 0 or len(self.previous_queue) > 0:
            #     BIA(self.current_queue, self.previous_queue, self.matched_queue, self.structure_detection, self.content_detection, self.presentation_detection, self.behaviour_detection).inspect()
            #
            #     # Duplicate removal
            #     print("\n[DIFF-MAIN] Duplicate removal procedure")
            #     print("Matched queue")
            #     self.matched_queue = utilities.removeDuplicates(self.matched_queue, True)
            #     print("Structural - Content - Presentation - Behavioural queues")
            #     self.structure_detection = utilities.removeDuplicates(self.structure_detection, True)
            #     self.content_detection = utilities.removeDuplicates(self.content_detection, True)
            #     self.presentation_detection = utilities.removeDuplicates(self.presentation_detection, True)
            #     self.behaviour_detection = utilities.removeDuplicates(self.behaviour_detection, True)
            #     self.showQueueMeta()
            #
            # else:
            #     print("[DIFF-MAIN :: Processing] After LOC, there is no elements in current/previous queues. Exiting...")
            #     exit(-2)

            # Phase 2 - #4 alter BIA
            if len(self.current_queue) > 0 or len(self.previous_queue) > 0:
                aBIA(self.current_queue, self.previous_queue, self.matched_queue, self.structure_detection, self.content_detection, self.presentation_detection, self.behaviour_detection).inspect()

                # Duplicate removal
                print("\n[DIFF-MAIN] Duplicate removal procedure")
                print("Matched queue")
                self.matched_queue = utilities.removeDuplicates(self.matched_queue, True)
                print("Structural - Content - Presentation - Behavioural queues")
                self.structure_detection = utilities.removeDuplicates(self.structure_detection, True)
                self.content_detection = utilities.removeDuplicates(self.content_detection, True)
                self.presentation_detection = utilities.removeDuplicates(self.presentation_detection, True)
                self.behaviour_detection = utilities.removeDuplicates(self.behaviour_detection, True)
                self.showQueueMeta()

            else:
                print("[DIFF-MAIN :: Processing] After LOC, there is no elements in current/previous queues. Exiting...")
                exit(-2)

            # [self.matched_queue, self.structure_detection, self.content_detection, self.presentation_detection,, self.behaviour_detection]
            return [self.matched_queue, self.structure_detection, self.content_detection, self.presentation_detection, self.behaviour_detection]
        else:
            print("[DIFF-MAIN :: Processing] Input queues for processing-class are messed up! Exiting...")
            exit(-2)
