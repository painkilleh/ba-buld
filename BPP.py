# BPP - stands for behavioural pre-processing
# Main idea - to find Embedding JavaScript in HTML (docstore.mik.ua/orelly/webprog/jscript/ch12_02.htm) with respect to underlying technology

# Client-side JavaScript code is embedded within HTML documents in a number of ways:
# 1. Between a pair of tags <script> and </script>
# 2. From an external file specified by the src attribute of a <script> tag
# 3. In an event handler, specified as the value of an HTML attribute such as onclick or onmouseover (implemented in event-handler.py)

import networkx as nx
import utils as utilities
from lxml import etree


class BPP:

    def __init__(self, current_queue, previous_queue, behaviour_detection):
        self.current_queue: list = current_queue
        self.previous_queue: list = previous_queue
        self.behaviour_detection: list = behaviour_detection

    # Function that extracts children for selected element (recursive)
    def getChildNodes(self, element, queue):

        # Local variables for storing data
        temp_graph: nx.DiGraph = element[3]
        local_storage: list = []
        local_iter = iter(local_storage)

        # Extracting data from first element (1st - level -> direct successors)
        if temp_graph.number_of_nodes() > 1:
            for node_id in temp_graph.successors(element[0]):

                tmp_node = utilities.searchinqueue(node_id, queue, False, False)

                if isinstance(tmp_node, list):
                    local_storage.append(tmp_node)

        # Recursion
        while True:
            try:
                # Extracting element's graph from already added elements
                internal_element = next(local_iter)
                temp_graph = internal_element[3]

                if temp_graph.number_of_nodes() > 1:
                    for node_id in temp_graph.successors(internal_element[0]):

                        tmp_node = utilities.searchinqueue(node_id, queue, False, False)

                        if isinstance(tmp_node, list):
                            local_storage.append(tmp_node)

            except StopIteration:
                break

        return local_storage

    # Find the redirects
    def findRedirects(self):
        # Inits
        current_head_element: list = None
        previous_head_element: list = None

        current_buffer: list = []
        previous_buffer: list = []

        # Specific tag belongs to head - tag only. Therefore, looking for head tag (https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta)
        for c_element in self.current_queue:

            current_node = c_element[3].nodes[c_element[0]]

            if current_node["tag"] == "head":
                current_head_element = c_element
                break

        for p_element in self.previous_queue:

            previous_node = p_element[3].nodes[p_element[0]]

            if previous_node["tag"] == "head":
                previous_head_element = p_element
                break

        if current_head_element is not None:
            # Extracting all children of head-tag element
            current_head_children = self.getChildNodes(current_head_element, self.current_queue)
            # print("Extracted current children (size: %s): %s" % (len(current_head_children), current_head_children))

            # Iterating through children list to find specific tag with specific parameters
            for current_element in current_head_children:

                current_child_node = current_element[3].nodes[current_element[0]]

                # Looking for specific tag + options and its attributes/parameters
                if current_child_node["tag"] == "meta":

                    node_attrs: dict = current_child_node["attrs"]

                    if "http-equiv" in node_attrs:

                        if node_attrs["http-equiv"] == "refresh":

                            if "content" in node_attrs:

                                redirect_parameter: str = node_attrs["content"]

                                # Looking for delimiter of parameters and url option -> sec<int> ; url=<url>
                                if ";" in redirect_parameter and "url=" in redirect_parameter:
                                    current_buffer.append(current_element)

        if previous_head_element is not None:

            previous_head_children = self.getChildNodes(previous_head_element, self.previous_queue)
            # print("Extracted previous children (size: %s): %s" % (len(previous_head_children), previous_head_children))

            for previous_element in previous_head_children:

                previous_child_node = previous_element[3].nodes[previous_element[0]]

                # Looking for specific tag + options and its attributes/parameters
                if previous_child_node["tag"] == "meta":

                    node_attrs: dict = previous_child_node["attrs"]

                    if "http-equiv" in node_attrs:

                        if node_attrs["http-equiv"] == "refresh":

                            if "content" in node_attrs:

                                redirect_parameter: str = node_attrs["content"]

                                # Looking for delimiter of parameters and url option -> sec<int> ; url=<url>
                                if ";" in redirect_parameter and "url=" in redirect_parameter:
                                    previous_buffer.append(previous_element)

        return [current_buffer, previous_buffer]

    # Find the refreshers
    def findRefreshers(self):
        # Inits
        current_head_element: list = None
        previous_head_element: list = None

        current_buffer: list = []
        previous_buffer: list = []

        # Specific tag belongs to head - tag only. Therefore, looking for head tag (https://developer.mozilla.org/en-US/docs/Web/HTML/Element/meta)
        for c_element in self.current_queue:

            current_node = c_element[3].nodes[c_element[0]]

            if current_node["tag"] == "head":
                current_head_element = c_element
                break

        for p_element in self.previous_queue:

            previous_node = p_element[3].nodes[p_element[0]]

            if previous_node["tag"] == "head":
                previous_head_element = p_element
                break

        if current_head_element is not None:

            # Extracting all children of head-tag element
            current_head_children = self.getChildNodes(current_head_element, self.current_queue)
            # print("Extracted current children (size: %s): %s" % (len(current_head_children), current_head_children))

            for current_element in current_head_children:

                current_child_node = current_element[3].nodes[current_element[0]]

                # Looking for specific tag + options and its attributes/parameters
                if current_child_node["tag"] == "meta":

                    node_attrs: dict = current_child_node["attrs"]

                    if "http-equiv" in node_attrs:

                        if node_attrs["http-equiv"] == "refresh":

                            if "content" in node_attrs:

                                redirect_parameter: str = node_attrs["content"]

                                # Without url options - it is just refresher of the page
                                if str.isdigit(redirect_parameter) is True:
                                    current_buffer.append(current_element)

        if previous_head_element is not None:

            previous_head_children = self.getChildNodes(previous_head_element, self.previous_queue)
            # print("Extracted previous children (size: %s): %s" % (len(previous_head_children), previous_head_children))

            for previous_element in previous_head_children:

                previous_child_node = previous_element[3].nodes[previous_element[0]]

                # Looking for specific tag + options and its attributes/parameters
                if previous_child_node["tag"] == "meta":

                    node_attrs: dict = previous_child_node["attrs"]

                    if "http-equiv" in node_attrs:

                        if node_attrs["http-equiv"] == "refresh":

                            if "content" in node_attrs:

                                redirect_parameter: str = node_attrs["content"]

                                # Without url options - it is just refresher of the page
                                if str.isdigit(redirect_parameter) is True:
                                    previous_buffer.append(previous_element)

        return [current_buffer, previous_buffer]

    # List differences for redirects
    def analyseRedirects(self, composite_buffer):
        # Buffer for the analysis
        current_buffer: list = composite_buffer[0]
        previous_buffer: list = composite_buffer[1]

        curr_iter = iter(current_buffer)

        print("[BPP - Redirects] Start of analysis")

        while True:

            try:
                c_element = next(curr_iter)
            except StopIteration:
                break

            # Filtering identical redirect notations
            prev_iter = iter(previous_buffer)
            c_node = c_element[3].nodes[c_element[0]]
            c_attrs = c_node["attrs"]

            while True:
                try:
                    p_element = next(prev_iter)
                except StopIteration:
                    break

                p_node = p_element[3].nodes[p_element[0]]
                p_attrs = p_node["attrs"]

                if p_node["tag"] == c_node["tag"]:

                    if c_attrs["http-equiv"] == p_attrs["http-equiv"]:

                        if c_attrs["content"] == p_attrs["content"]:

                            print("[BPP - Redirects] Identical elements found (UIDs: %s , %s)" % (c_element[0], p_element[0]))

                            # Checking UIDs (at this moment, the identical elements are found)
                            if c_element[0] != p_element[0]:
                                status = "redirect's element allocation modified"
                                current_buffer.remove(c_element)
                                previous_buffer.remove(p_element)
                                self.behaviour_detection.append([c_element, p_element, status])
                                break
                            else:
                                current_buffer.remove(c_element)
                                previous_buffer.remove(p_element)
                                break

        # Classifying remaining
        if len(current_buffer) > 0:
            print("[BPP - Redirects] New redirects count: ", len(current_buffer))
            for element in current_buffer:
                status = "new redirect"
                self.behaviour_detection.append([element, status])

        if len(previous_buffer) > 0:
            print("[BPP - Redirects] New redirects count: ", len(previous_buffer))
            for element in previous_buffer:
                status = "removed redirect"
                self.behaviour_detection.append([element, status])

    # List differences for refresher
    def analyseRefreshers(self, composite_buffer):
        # Buffer for the analysis
        current_buffer: list = composite_buffer[0]
        previous_buffer: list = composite_buffer[1]

        curr_iter = iter(current_buffer)

        print("[BPP - Refreshers] Start of analysis")

        while True:

            try:
                c_element = next(curr_iter)
            except StopIteration:
                break

            # Filtering identical redirect notations
            prev_iter = iter(previous_buffer)
            c_node = c_element[3].nodes[c_element[0]]
            c_attrs = c_node["attrs"]

            while True:
                try:
                    p_element = next(prev_iter)
                except StopIteration:
                    break

                p_node = p_element[3].nodes[p_element[0]]
                p_attrs = p_node["attrs"]

                if p_node["tag"] == c_node["tag"]:

                    if c_attrs["http-equiv"] == p_attrs["http-equiv"]:

                        if c_attrs["content"] == p_attrs["content"]:

                            print("[BPP - Refreshers] Identical elements found (UIDs: %s, %s)" % (c_element[0], p_element[0]))

                            # Checking UIDs (at this moment, the identical elements are found)
                            if c_element[0] != p_element[0]:
                                status = "content refresher's element allocation modified"
                                current_buffer.remove(c_element)
                                previous_buffer.remove(p_element)
                                self.behaviour_detection.append([c_element, p_element, status])
                                break
                            else:
                                current_buffer.remove(c_element)
                                previous_buffer.remove(p_element)
                                break

        # Classifying remaining
        if len(current_buffer) > 0:
            print("[BPP - Refreshers] New refreshers count: ", len(current_buffer))
            for element in current_buffer:
                status = "new content refresher"
                self.behaviour_detection.append([element, status])

        if len(previous_buffer) > 0:
            print("[BPP - Refreshers] Removed refreshers count: ", len(previous_buffer))
            for element in previous_buffer:
                status = "removed content refresher"
                self.behaviour_detection.append([element, status])

    # Looking for first condition (internal scripts)
    def findEnclosedScript(self):

        current_buffer: list = []
        previous_buffer: list = []

        for c_element in self.current_queue:

            c_node = c_element[3].nodes[c_element[0]]

            # Checking for tag + content
            if c_node["tag"] == "script" and c_node["weight"] > 0:
                # print( "[BPP - ES] Enclosed script found in current queue [UID: %s, tag: %s, weight: %s]" % (c_element[0], c_node["tag"], c_node["weight"]) )
                # print( "[BPP - ES] Content for current active component:  %s" % c_node["tag_content"] )
                # print()
                current_buffer.append(c_element)

        for p_element in self.previous_queue:

            p_node = p_element[3].nodes[p_element[0]]

            if p_node["tag"] == "script" and p_node["weight"] > 0:
                # print( "[BPP - ES] Enclosed script found in previous queue [UID: %s, tag: %s, weight: %s]" % (p_element[0], p_node["tag"], p_node["weight"]) )
                # print( "[BPP - ES] Content for previous active component: %s" % p_node["tag_content"] )
                # print()
                previous_buffer.append(p_element)

        return [current_buffer, previous_buffer]

    # Looking for second condition (external scripts)
    def findExternalScript(self):

        current_buffer: list = []
        previous_buffer: list = []

        for c_element in self.current_queue:

            c_node = c_element[3].nodes[c_element[0]]

            # Checking for tag
            if c_node["tag"] == "script":

                # Extracting attributes as dictionary
                attributes: dict = c_node["attrs"]

                if "src" in attributes and attributes.get("src") is not None and c_element[2] == 0:
                    # print("[BPP - ExS] External script found in current queue [UID: %s, tag: %s, source : %s]" % (c_element[0], c_node["tag"], c_node["attrs"]["src"]))
                    # print( "[BPP - ExS] DOM for current active component: %s" % c_node["DOM"] )
                    # print()
                    current_buffer.append(c_element)

        for p_element in self.previous_queue:

            p_node = p_element[3].nodes[p_element[0]]

            # Checking for tag
            if p_node["tag"] == "script":

                # Extracting attributes as dictionary
                attributes: dict = p_node["attrs"]

                if "src" in attributes and attributes.get("src") is not None and p_element[2] == 0:
                    # print("[BPP - ExS] External script found in previous queue [UID: %s, tag: %s, source : %s]" % (p_element[0], p_node["tag"], p_node["attrs"]["src"]))
                    # print("[BPP - ExS] Content for previous active component: %s" % p_node["DOM"])
                    # print()
                    previous_buffer.append(p_element)

        return [current_buffer, previous_buffer]

    # List differences for enclosed scripts
    def analyseES(self, composite_buffer):
        # Local inits
        current_buffer: list = composite_buffer[0]
        previous_buffer: list = composite_buffer[1]

        curr_iter = iter(current_buffer)

        print("[BPP - EnS] Start of analysis")

        while True:

            try:
                c_element = next(curr_iter)
            except StopIteration:
                break

            c_node = c_element[3].nodes[c_element[0]]
            prev_iter = iter(previous_buffer)

            while True:

                try:
                    p_element = next(prev_iter)
                except StopIteration:
                    break

                p_node = p_element[3].nodes[p_element[0]]

                # Checking for similarity
                if c_node["SHA_content"] == p_node["SHA_content"] and c_element[2] == p_element[2]:

                    print("[BPP - EnS] Identical elements found! (UIDs: %s , %s) " % (c_element[0], p_element[0]))
                    print("[BPP - Ens] Content checksums (new/old) : % s -- %s" % (c_node["SHA_content"], p_node["SHA_content"]))

                    print("[BPP - EnS] DOM object (new/old) : %s \n %s" % (c_node["DOM"], p_node["DOM"]))

                    # Checking UIDs - since its position (discovery index) defines its execution sequence within document
                    if c_element[0] != p_element[0]:
                        status = "enclosed script element allocation modified"
                        self.behaviour_detection.append([c_element, p_element, status])
                        current_buffer.remove(c_element)
                        previous_buffer.remove(p_element)
                        break
                    else:
                        current_buffer.remove(c_element)
                        previous_buffer.remove(p_element)
                        break

        # Checking the leftovers
        if len(current_buffer) > 0:
            print("[BPP - EnS] New enclosed script count: ", len(current_buffer))
            for c_element in current_buffer:
                status = "new enclosed script"
                self.behaviour_detection.append([c_element, status])

        if len(previous_buffer) > 0:
            print("[BPP - EnS] Removed enclosed script count: ", len(previous_buffer))
            for p_element in previous_buffer:
                status = "removed enclosed script"
                self.behaviour_detection.append([p_element, status])

    # List differences for external scripts
    def analyseExS(self, composite_buffer):
        # Local inits
        current_buffer: list = composite_buffer[0]
        previous_buffer: list = composite_buffer[1]

        curr_iter = iter(current_buffer)

        print("[BPP - ExS] Start of analysis")

        while True:

            try:
                c_element = next(curr_iter)
            except StopIteration:
                break

            c_node = c_element[3].nodes[c_element[0]]
            c_attrs = c_node["attrs"]
            prev_iter = iter(previous_buffer)

            while True:

                try:
                    p_element = next(prev_iter)
                except StopIteration:
                    break

                p_node = p_element[3].nodes[p_element[0]]
                p_attrs = p_node["attrs"]

                # Checking for similarity
                if c_attrs["src"] == p_attrs["src"]:

                    print("[BPP - ExS] Identical elements found! (UIDs: %s, %s)" % (c_element[0], p_element[0]))
                    # Checking UIDs - since its position (discovery index) defines its execution sequence within document
                    if c_element[0] != p_element[0]:
                        status = "external script element allocation modified"
                        self.behaviour_detection.append([c_element, p_element, status])
                        current_buffer.remove(c_element)
                        previous_buffer.remove(p_element)
                        break
                    else:
                        current_buffer.remove(c_element)
                        previous_buffer.remove(p_element)
                        break

        # Checking the leftovers
        if len(current_buffer) > 0:
            print("[BPP - ExS] New external script count: ", len(current_buffer))
            for c_element in current_buffer:
                status = "new external script"
                self.behaviour_detection.append([c_element, status])

        if len(previous_buffer) > 0:
            print("[BPP - ExS] Removed external script count: ", len(previous_buffer))
            for p_element in previous_buffer:
                status = "removed external script"
                self.behaviour_detection.append([p_element, status])

    # Find forms/button tags
    def findForms(self):
        # Inits
        current_buffer: list = []
        previous_buffer: list = []

        for c_element in self.current_queue:

            current_selected_node = c_element[3].nodes[c_element[0]]

            if current_selected_node["tag"] == "form":

                current_attrs = current_selected_node["attrs"]

                if "method" in current_attrs or "action" in current_attrs:
                    current_buffer.append(c_element)

        for p_element in self.previous_queue:

            previous_selected_node = p_element[3].nodes[p_element[0]]

            if previous_selected_node["tag"] == "form":

                previous_attrs = previous_selected_node["attrs"]

                if "method" in previous_attrs or "action" in previous_attrs:
                    previous_buffer.append(p_element)

        return [current_buffer, previous_buffer]

    def findButtons(self):
        # Inits
        current_buffer: list = []
        previous_buffer: list = []

        for c_element in self.current_queue:

            current_selected_node = c_element[3].nodes[c_element[0]]

            if current_selected_node["tag"] == "button":

                current_attrs = current_selected_node["attrs"]

                if "formaction" in current_attrs or "formmethod" in current_attrs or "type" in current_attrs:
                    current_buffer.append(c_element)

        for p_element in self.previous_queue:

            previous_selected_node = p_element[3].nodes[p_element[0]]

            if previous_selected_node["tag"] == "button":

                previous_attrs = previous_selected_node["attrs"]

                if "formaction" in previous_attrs or "formmethod" in previous_attrs or "type" in previous_attrs:
                    previous_buffer.append(p_element)

        return [current_buffer, previous_buffer]

    # List differences for forms
    def analyseForms(self, composite_buffer):
        # Inits
        current_buffer: list = composite_buffer[0]
        previous_buffer: list = composite_buffer[1]

        curr_iter = iter(current_buffer)

        print("[BPP - Forms] Start of analysis")

        while True:

            try:
                c_element = next(curr_iter)
            except StopIteration:
                break

            c_node = c_element[3].nodes[c_element[0]]
            c_attrs: dict = c_node["attrs"]
            prev_iter = iter(previous_buffer)

            while True:

                try:
                    p_element = next(prev_iter)
                except StopIteration:
                    break

                p_node = p_element[3].nodes[p_element[0]]
                p_attrs: dict = p_node["attrs"]

                # Checking for similarity
                if c_attrs.get("method") == p_attrs.get("method") and c_attrs.get("action") == p_attrs.get("action"):

                    print("[BPP - Forms] Identical elements found! (UIDs: %s, %s)" % (c_element[0], p_element[0]))

                    if (c_attrs.get("method") is None and p_attrs.get("method") is None) or (c_attrs.get("action") is None and p_attrs.get("action") is None) is False:
                        # Checking UIDs - since its position (discovery index) defines its execution sequence within document
                        if c_element[0] != p_element[0]:
                            status = "form element allocation modified"
                            self.behaviour_detection.append([c_element, p_element, status])
                            current_buffer.remove(c_element)
                            previous_buffer.remove(p_element)
                            break
                        else:
                            current_buffer.remove(c_element)
                            previous_buffer.remove(p_element)
                            break

        # Checking the leftovers
        if len(current_buffer) > 0:
            print("[BPP - Forms] Added form behaviour count: ", len(current_buffer))
            for c_element in current_buffer:
                status = "form behaviour added"
                self.behaviour_detection.append([c_element, status])

        if len(previous_buffer) > 0:
            print("[BPP - Forms] Modified form behaviour count: ", len(previous_buffer))
            for p_element in previous_buffer:
                status = "form behaviour modified"
                self.behaviour_detection.append([p_element, status])

    # Analyse buttons
    def analyseButtons(self, composite_buffer):
        # Inits
        current_buffer: list = composite_buffer[0]
        previous_buffer: list = composite_buffer[1]

        curr_iter = iter(current_buffer)

        print("[BPP - Buttons] Start of analysis")

        while True:

            try:
                c_element = next(curr_iter)
            except StopIteration:
                break

            c_node = c_element[3].nodes[c_element[0]]
            c_attrs: dict = c_node["attrs"]
            prev_iter = iter(previous_buffer)

            while True:

                try:
                    p_element = next(prev_iter)
                except StopIteration:
                    break

                p_node = p_element[3].nodes[p_element[0]]
                p_attrs: dict = p_node["attrs"]

                # Checking for similarity
                if c_attrs.get("formaction") == p_attrs.get("formaction") and c_attrs.get("formmethod") == p_attrs.get("formmethod") and c_attrs.get("type") == p_attrs.get("type"):

                    print("[BPP - Buttons] Identical elements found! (UIDs: %s, %s)" % (c_element[0], p_element[0]))

                    if (c_attrs.get("formaction") is None and p_attrs.get("formaction") is None) or (c_attrs.get("formmethod") is None and p_attrs.get("formmethod") is None) or (c_attrs.get("type") is None and p_attrs.get("type") is None) is False:
                        # Checking UIDs - since its position (discovery index) defines its execution sequence within document
                        if c_element[0] != p_element[0]:
                            status = "button element allocation modified"
                            self.behaviour_detection.append([c_element, p_element, status])
                            current_buffer.remove(c_element)
                            previous_buffer.remove(p_element)
                            break
                        else:
                            current_buffer.remove(c_element)
                            previous_buffer.remove(p_element)
                            break

        # Checking the leftovers
        if len(current_buffer) > 0:
            print("[BPP - Forms] Added button behaviour count: ", len(current_buffer))
            for c_element in current_buffer:
                status = "button behaviour added"
                self.behaviour_detection.append([c_element, status])

        if len(previous_buffer) > 0:
            print("[BPP - Forms] Modified button behaviour count: ", len(previous_buffer))
            for p_element in previous_buffer:
                status = "button behaviour modified"
                self.behaviour_detection.append([p_element, status])

    # Main execution
    def analyse(self):
        print("[BPP] Start of execution")

        self.analyseRedirects(self.findRedirects())
        self.analyseRefreshers(self.findRefreshers())
        self.analyseES(self.findEnclosedScript())
        self.analyseExS(self.findExternalScript())
        self.analyseForms(self.findForms())
        self.analyseButtons(self.findButtons())
