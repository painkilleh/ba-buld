# Phase 1 - Pre-processing
# Class that is responsible for sequential execution of pre-processing sub-classes

import tree_generation as tg
import priority_queue as pq
import networkx as nx


class PreProcessing:
    # Inits
    def __init__(self, current_path, previous_path):
        self.current_path = current_path
        self.previous_path = previous_path

    # Main call
    def execute(self):
        # Phase 1 - #1 & #2 (Tree generation & hashing)
        current_tree: nx.DiGraph = tg.TreeGeneration(self.current_path).convert()
        previous_tree: nx.DiGraph = tg.TreeGeneration(self.previous_path).convert()

        # Checking if function returned tree structure
        if isinstance(current_tree, nx.DiGraph) and isinstance(previous_tree, nx.DiGraph):
            # Phase 1 - #3 (Priority queue)
            both_queues = pq.PriorityQueue(current_tree, previous_tree).organize()

            # Checking if function returned formulated queues
            if (isinstance(both_queues[0], list) and isinstance(both_queues[1], list)) and (both_queues[0] is not None and both_queues[1] is not None):
                return both_queues
            else:
                print("[DIFF-MAIN :: PreProcessing] Failed to return formulated queues! Exiting...")
                exit(-1)
        else:
            print("[DIFF-MAIN :: PreProcessing] Failed to return tree structure! Exiting...")
            exit(-1)
