# Change detection in web applications

This project is devoted to the change detection in dynamic web application. The work is based on BULD-diff algorithm with additional expansions that provide
better detection classification for changes.

## Taxonomy of changes in web-based applications

The algorithm recognizes following lists of possible changes between two version of web application codes in comparison:

- Structural changes (insertion, removal, change of position in hierarchy of DOM objects)
- Presentational changes (representation deviation of two DOM object in comparison)
- Content changes (textual information deviation of two DOM objects in comparison)
- Behavioural changes (behavioural changes that affects the functionality of the web-application/DOM objects)

## Phases of diff-algorithm

The detection algorithm is divided into 3 main phases - pre-processing, processing and post-processing. All these main phases contain several sub-phases that 
has its own responsibility for the phase.

- **Phase 1** (Pre-processing) : code parsing, code conversion into tree, data registration and signature calculation, conversion of tree into queue
- **Phase 2** (Processing) : main analysis / decision-making procedures 
- **Phase 3** (Post-processing) : delta file generation

## Preparation for algorithm's execution

The algorithm is written and tested on Python 3.7.3. In the current repository, there is a text file that contains information of all supportive libraries that
were used during the development (requirements.txt).

To make this algorithm work on computing device, the user must have or install Python 3.7+ from official website. In addition, the user must install pip.
Save current repository on your local machine and run the command in local directory of the project:

- pip install -r requirements.txt

To start comparison of two web-based application codes, open the _diff.py_ file and find the variables _curr_file_ , _prev_file_. Those variables represent
the paths to web-based code application files (HTML files). The _curr_file_ variable represents the current iteration of the code (most recent one/in production), 
_prev_file_ variable - the last iteration of the code available. After the paths are provided for _diff.py_, just execute the _diff.py_.

---