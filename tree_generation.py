# Phase #1 - 1 & 2 (Tree generation & hashing)
# Purpose: Converts HTML-based file into the tree structure

import hashlib

import lxml.etree as et
import networkx as nx
from bs4 import BeautifulSoup, Tag, NavigableString, Comment
from networkx import DiGraph


class TreeGeneration:
    # Inits
    def __init__(self, f_path):
        self.path: str = f_path

    def convert(self):

        raw_data: str = ""
        line_buffer = []

        # Opening the file HTMl file
        with open(self.path, "r", encoding="utf-8") as input_file:
            line_buffer = input_file.readlines()

        for line in line_buffer:
            raw_data += " ".join(line.split())

        # print("LB: ", line_buffer)
        # print("RD: ", raw_data)
        # exit(-1)

        # Tree init
        generated_tree: DiGraph = nx.DiGraph()

        # Removing comments/blanks/etc. that are not essential for parsing HTML document
        form_page = et.fromstring(raw_data, parser=et.HTMLParser(encoding="utf-8", remove_comments=True, huge_tree=True, compact=False))

        # Removing /n/t notations in string + fixing the decoding problems (b'<text>' literals)
        last_convert = et.tostring(form_page, method="html").decode("utf-8")

        # Applying module to parse HTML-page
        parsepage = BeautifulSoup(last_convert, features="lxml")

        # Function that parses through all elements in document
        def register_nodes():
            # Traverse only if it is a Tag-type object
            def parse_object(dom_obj, parent_uid, parent_graph):
                if isinstance(dom_obj, Tag):
                    graph_node = dom_inspector(dom_obj)
                    graph_node.insert(0, len(list(generated_tree.nodes)))
                    generated_tree.add_node(graph_node[0], tag=graph_node[1], attrs=graph_node[2], SHA_label=graph_node[3], tag_content=graph_node[4], SHA_content=graph_node[5], DOM=graph_node[6], SHA_DOM=graph_node[7], weight=graph_node[8], parent_uid=None)

                    # Appending parent_uid to the node if it is required
                    if parent_uid is not None:
                        generated_tree.add_edge(parent_graph[0], graph_node[0])
                        generated_tree.nodes[graph_node[0]]["parent_uid"] = parent_graph[0]

                    # Exhaustive function to process and register all the nodes in file
                    for child in dom_obj.children:
                        if isinstance(child, Tag):
                            parse_object(child, dom_obj, graph_node)
            # Actual call
            parse_object(parsepage, None, None)

        # Function that inspects selected DOM object and processes/registers the data into the graph
        def dom_inspector(dom):
            if isinstance(dom, Tag):
                sha_label = hashlib.sha256()
                sha_content = hashlib.sha256()
                sha_dom = hashlib.sha256()

                tag_content: str = ""

                # Content of the tag only, excluding children
                tag_strings = [string for string in dom if isinstance(string, NavigableString)]

                # Removing whitespaces and unrelated information -> trimming
                for row in tag_strings:
                    if row:
                        row = row.lstrip()
                        row = row.rstrip()
                        row = row.replace("\r", "")
                        tag_content += row

                # print("Final content: ", tag_content)
                sha_label.update(str(dom.name).encode(encoding="utf-8"))
                sha_label.update(str(dom.attrs).encode(encoding="utf-8"))

                sha_dom.update(str(dom).encode(encoding="utf-8"))
                weight = len(dom.text)

                if len(tag_content) != 0:
                    sha_content.update(str(tag_content).encode(encoding="utf-8"))
                    processed_block = [dom.name, dom.attrs, sha_label.hexdigest(), tag_content, sha_content.hexdigest(), dom, sha_dom.hexdigest(), weight]
                else:
                    sha_content = 0
                    processed_block = [dom.name, dom.attrs, sha_label.hexdigest(), tag_content, sha_content, dom, sha_dom.hexdigest(), weight]
                return processed_block
            elif isinstance(dom, NavigableString):
                # print( "NavigableString-type: ", DOM.string )
                pass
            elif isinstance(dom, Comment):
                # print( "Comment-type: ", DOM )
                pass
            else:
                # Just in case if unexpected input
                return "Unsupported type of data!"

        # Actual execution
        register_nodes()

        return generated_tree
