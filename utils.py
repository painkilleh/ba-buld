# Supportive functions for detection phases

import networkx as nx
import matplotlib.pyplot as plt


def extractdatafromgraph(uid, graph):
    """
    Function that returns node from graph
    :param uid: UID of node for extraction
    :param graph: graph for the node
    :return: node that contain all the information (uid, tag, attributes and so on)
    """
    # if isinstance(graph, nx.DiGraph):
    #     root_index = find_root(graph, index)
    #     # print("[EXTRACT function] Root index: ", root_index)
    #     # print("[EXTRACT] Has node : ", Graph.has_node(index))
    #     root_node = graph.nodes[root_index]
    #     # print("[EXTRACT function] Root node", root_node)
    #     return root_node

    if isinstance(graph, nx.DiGraph) is True and isinstance(uid, int):
        selected_node = graph.nodes[uid]
        return selected_node
    else:
        print("[EDFG] Wrong parameters!")


def searchinqueue(uid, queue, return_pair, matched):
    """
    Searches for specific element in queue
    :param uid: Node's UID
    :param queue: Queue to search in
    :param return_pair: Return pair for matched array
    :param matched: select matched array position number (0 - current; 1 - previous) only works with return pair parameter
    :return: element of queue/pair if element was found in queue, False - otherwise
    """

    if isinstance(uid, int):
        if isinstance(queue, list):

            if return_pair is False:
                # Block for regular queues (current, previous)
                for element in queue:
                    if uid == element[0]:
                        return element
                return False
            else:
                if (isinstance(matched, int) is True) and (matched == 0 or matched == 1):

                    for element_pair in queue:
                        element = element_pair[matched]

                        if element[0] == uid:
                            return element_pair

                    return False

                else:
                    print("[SIQ] Matched-parameter is wrongly!")
        else:
            print("[SIQ] Queue is not list-type!")
    else:
        print("[SIQ] UID is not int-type!")


def comparedocumentnodes(current_document_root, previous_document_root):
    """
    Compares uid (0) or root-node for changes that are responsible for whole file
    :param current_document_root: current element of queue that contains root element (UID: 0)
    :param previous_document_root: previous element of queue that contains root element (UID: 0)
    :return: True - if both elements are identical (files are the same), False - if some parameters (UID,
    parent UID, tag, weight (amount of symbols in file) and signature of whole DOM object)
    """
    # print( "Comparing document-nodes for both queues that represent files, format: [current/previous]..." )
    # print( current_document_root, previous_document_root )

    print("[WDA - Document] Inspecting document-nodes..")

    # Checking indexes
    if current_document_root[0] == previous_document_root[0]:
        # print( "Nodes indexes are the same! Indexes [%s, %s]" % (current_document_root[0], previous_document_root[0]) )

        # Checking overall weight of the document
        if current_document_root[2] == previous_document_root[2]:
            # print("[Document] Overall content size is the same! Size [%s, %s]" % (current_document_root[2], previous_document_root[2]))

            # For root-node only
            # Extracting graph (or sub graph) from queue to analyse
            curr_graph = current_document_root[3]
            prev_graph = previous_document_root[3]

            # Extracting root-node from graph
            current_root_node = extractdatafromgraph(curr_graph, current_document_root[0])
            previous_root_node = extractdatafromgraph(prev_graph, previous_document_root[0])

            # Extracting data from root-node in Graph for both queues
            # Checking overall tag-name (should be <[document]>]
            if current_root_node["tag"] == previous_root_node["tag"]:
                # Checking integrity of tag's content
                # Checking integrity of whole DOM object that represents the document
                if current_root_node["SHA_DOM"] == previous_root_node["SHA_DOM"]:
                    # print("[Document] Document nodes for both files are identical!")
                    return True
                else:
                    # print("[Document] Document nodes are not the same!")
                    return False
            else:
                # print("[Document] DOM tags does not match! DOM-tags [%s, %s]" % (current_root_node["tag"], previous_root_node["tag"]))
                return False
        else:
            # print("[Document] Overall content size is NOT the same! Size [%s, %s]" % (current_document_root[2], previous_document_root[2]))
            return False
    else:
        return False


def find_root(graph, child):
    """
    Finds the root-node for the selected graph
    :param graph: desired graph
    :param child: node UID
    :return UID of the root node
    """
    parent = list(graph.predecessors(child))
    if len(parent) == 0:
        # print("Found root: ", child)
        return child
    else:
        return find_root(graph, parent[0])


def displayGraphMeta(graph):
    """
    Display general information about entered graph
    :param graph: graph to inspect
    """
    print("# of edges: ", graph.number_of_edges())
    print("# of nodes: ", graph.number_of_nodes())
    print(graph.nodes)
    print(graph.edges)
    nx.draw(graph, pos=nx.shell_layout(graph), with_labels=True)
    plt.draw()
    plt.show()


def removeDuplicates(queue, show):
    # Inits
    final_list = []
    counter = 0

    for num in queue:
        if num not in final_list:
            final_list.append(num)
        else:
            counter += 1
    if show is True:
        print("Duplicates removed: ", counter)
    return final_list


def inspectAttributesForSimilarity(current_attributes: dict, previous_attributes: dict):
    """
    Inspects two dictionaries of attributes for similarity
    :param current_attributes : current attributes of DOM object
    :param previous_attributes : previous attributes od DOM object
    :return True/False : depending on similarity
    """

    if len(current_attributes) != len(previous_attributes):
        return False

    for c_key, c_value in current_attributes.items():

        for p_key, p_value in previous_attributes.items():
            # Allocate key (attribute identifier)
            if c_key == p_key:

                if len(c_value) != len(p_value) or c_value != p_value:
                    return False
                else:
                    break
            else:
                return False

    return True
