# Branch Inspection Algorithm
# Based on distance metrics in Graph; purpose : to identify various changes in subtrees

import networkx as nx
from fuzzywuzzy import fuzz
from bs4 import NavigableString
from itertools import groupby
import utils as utilities
from event_handler import EHD


class BIA:

    # Class inits
    def __init__(self, current_queue, previous_queue, matched_queue, structure_detection, content_detection, presentation_detection, behaviour_detection):
        self.current_queue: list = current_queue
        self.previous_queue: list = previous_queue
        self.matched_queue: list = matched_queue

        # Type of changes
        self.structure_detection: list = structure_detection
        self.content_detection: list = content_detection
        self.presentation_detection: list = presentation_detection
        self.behaviour_detection: list = behaviour_detection

        # Internal array
        self.uid_holder: list = []

        # Initializing EH parser
        self.event_handler_parser = EHD()

    # Function for element extraction from queue - current/previous/matched
    def extractElementFromQueue(self, element_uid, mode):
        if isinstance(element_uid, int):
            if mode == 0:

                # Checking current queue
                for element in self.current_queue:
                    if element_uid == element[0]:
                        return [0, element]

                # Checking matched queue
                for pair in self.matched_queue:
                    current_element_from_pair = pair[mode]

                    if element_uid == current_element_from_pair[0]:
                        return [2, pair]

                # If could not find any queues
                return False

            elif mode == 1:

                # Checking current queue
                for element in self.previous_queue:
                    if element_uid == element[0]:
                        return [0, element]

                # Checking matched queue
                for pair in self.matched_queue:
                    current_element_from_pair = pair[mode]

                    if element_uid == current_element_from_pair[0]:
                        return [2, pair]

                # If could not find any queues
                return False

            else:
                print("[BIA - EE] Wrong mode selection!")
        else:
            print("[BIA - EE] ID is not int-type!")

    # Function that extracts children for selected element
    def getChildNodes(self, element, mode):

        # Local variables for storing data
        temp_graph: nx.DiGraph = element[3]
        local_storage: list = []
        local_iter = iter(local_storage)
        skip_flag = False

        # Document-root exception
        if element[0] == 0 and element[1] is None:
            skip_flag = True

        # Extracting data from first element (1st - level -> direct successors)
        if temp_graph.number_of_nodes() > 1 and skip_flag is False:
            for node_id in temp_graph.successors(element[0]):
                if mode == 0:

                    tmp_node = self.extractElementFromQueue(node_id, 0)

                    if isinstance(tmp_node, list):
                        local_storage.append(tmp_node)
                    else:
                        # print("[GCN] Something went wrong!")
                        # print( "1st DEBUG: ", tmp_node)
                        break

                elif mode == 1:

                    tmp_node = self.extractElementFromQueue(node_id, 1)

                    if isinstance(tmp_node, list):
                        local_storage.append(tmp_node)
                    else:
                        # print("[GCN] Something went wrong!")
                        # print( "1st DEBUG: ", tmp_node)
                        break

                else:
                    print("[GCN] Wrong mode!")
                    break

        # Recursion
        while True and skip_flag is False:
            try:
                internal_element = next(local_iter)

                # If matched child
                if internal_element[0] == 2:
                    if mode == 0:
                        element = internal_element[1][mode]
                        temp_graph = internal_element[1][mode][3]

                    elif mode == 1:
                        element = internal_element[1][mode]
                        temp_graph = internal_element[1][mode][3]

                # If regular child
                else:
                    element = internal_element[1]
                    temp_graph = internal_element[1][3]

                if temp_graph.number_of_nodes() > 1:
                    for node_id in temp_graph.successors(element[0]):
                        if mode == 0:

                            tmp_node = self.extractElementFromQueue(node_id, 0)

                            if isinstance(tmp_node, list):
                                local_storage.append(tmp_node)
                            else:
                                # print("[GCN] Something went wrong!")
                                # print( "Iter DEBUG: ", tmp_node)
                                break

                        elif mode == 1:

                            tmp_node = self.extractElementFromQueue(node_id, 1)

                            if isinstance(tmp_node, list):
                                local_storage.append(tmp_node)
                            else:
                                # print("[GCN] Something went wrong!")
                                # print( "Iter DEBUG: ", tmp_node )
                                break

                        else:
                            print("[GCN] Wrong mode!")
                            break

            except StopIteration:
                break

        return local_storage

    # Function that divides array of children into levels of array that corresponds to branch hierarchy
    def separateIntoLevels(self, storage, mode):
        # Inits
        level_holder = []

        for key, group in groupby(storage, key=lambda x: x[1][1] if (x[0] == 0 or x[0] == 1) else x[1][mode][1]):
            level_holder.append(list(group))

        return level_holder

    # Function that deletes level and its related children from level storage
    def removeDependantLevels(self, element_uid, level_storage, mode):

        # print("ENTRY DATA: ", element_uid)
        start = 0
        end = len(self.uid_holder)

        # print("[RDL] Storage before (len: %s): \n%s" % (len(level_storage), level_storage))

        for level in level_storage:
            # Taking the first element in level storage -> defining element for level
            first_element_of_level = level[0]

            # print("First element on the level: ", first_element_of_level)

            # Classification of element
            # Regular queues

            if first_element_of_level[0] == 0:
                level_identity = first_element_of_level[1][1]
                # print( "LEVEL IDENTITY: ", level_identity )
            # Matched queue
            else:
                level_identity = first_element_of_level[1][mode][1]
                # print( "LEVEL IDENTITY: ", level_identity )

            # If level identification matches the
            if element_uid == level_identity:
                # Gathering children IDs to find another related element

                # print("[RDL-DEBUG] Comparison values: ", element_uid, level_identity)

                for element in level:
                    # Classification of element
                    # Regular queues
                    if element[0] == 0:
                        uid = element[1][0]
                        # print("REGULAR EXTRACTED UID: ", uid)
                        self.uid_holder.append(uid)
                    # Matched queue
                    else:
                        uid = element[1][mode][0]
                        # print( "MATCHED EXTRACTED UID: ", uid )
                        self.uid_holder.append(uid)

                # Removing the level
                # print("[RDL] Level to remove: ", level)
                level_storage.remove(level)

        # Recursion
        while start != end:

            for uid in self.uid_holder:
                self.removeDependantLevels(uid, level_storage, mode)

            start = end
            end = len(self.uid_holder)

        # print("[RDL] Algorithm exhausted!")

        # print("[RDL] Storage after (len: %s): \n%s" % (len(level_storage), level_storage))
        # print("[RDL] UID HOLDER: ", self.uid_holder)
        self.uid_holder.clear()

    # Function that removes (false positives) elements which are affected by decomposition
    def removeDecomposedElementsFromQueues(self, element, mode):

        structure_occ = 0
        present_occ = 0
        content_occ = 0

        # Checking structural queue
        structure_queue_iter = iter(self.structure_detection)

        while True:

            try:
                structure_element = next(structure_queue_iter)
            except StopIteration:
                break

            if structure_element[mode] is not None and isinstance(structure_element[mode], list) is True and structure_element[mode] == element:
                self.structure_detection.remove(structure_element)
                structure_occ += 1

        # Checking presentation queue
        presentation_queue_iter = iter(self.presentation_detection)

        while True:

            try:
                presentation_element = next(presentation_queue_iter)
            except StopIteration:
                break

            if presentation_element[mode] == element:
                self.presentation_detection.remove(presentation_element)
                present_occ += 1

        # Checking content queue
        content_queue_iter = iter(self.content_detection)

        while True:

            try:
                content_element = next(content_queue_iter)
            except StopIteration:
                break

            if content_element[mode] == element:
                self.content_detection.remove(content_element)
                content_occ += 1

        # if structure_occ > 0:
        #     print("[BIA - RDE] Structural occurrences removed : ", structure_occ)
        #
        # if present_occ > 0:
        #     print("[BIA - RDE] Presentational occurrences removed : ", structure_occ)
        #
        # if content_occ > 0:
        #     print("[BIA - RDE] Content occurrences removed : ", structure_occ)

    # Function that decomposes element
    def decomposeElementQueue(self, elements_queue, mode):

        # print("[BIA - Decompose] Start of iteration")

        for element in elements_queue:

            if element[0] == 0:
                self.removeDecomposedElementsFromQueues(element[1], mode)
            elif element[0] == 2:
                # Extracting pair
                pair = element[1]
                current_element = pair[0]
                previous_element = pair[1]

                if pair in self.matched_queue:
                    self.matched_queue.remove(pair)
                else:
                    print("[BIA - DQE] Pair does not belong to the queue! Pair (%s)" % pair)

                if current_element not in self.current_queue:
                    self.current_queue.append(current_element)

                if previous_element not in self.previous_queue:
                    self.previous_queue.append(previous_element)

                self.removeDecomposedElementsFromQueues(current_element, 0)
                self.removeDecomposedElementsFromQueues(previous_element, 1)

    # Function that inspects two elements
    def inspectElements(self, current_element, previous_element, current_subbranch_levels, previous_subbranch_levels):

        aloc_current_element = self.extractElementFromQueue(current_element[0], 0)
        aloc_previous_element = self.extractElementFromQueue(previous_element[0], 1)

        # Double checking the allocation of the element that is being sent to function -> it must be regular one
        if aloc_current_element[0] == 0 and aloc_previous_element[0] == 0:

            # Extracing the data
            current_graph: nx.DiGraph = current_element[3]
            previous_graph: nx.DiGraph = previous_element[3]

            current_node = current_graph.nodes[current_element[0]]
            previous_node = previous_graph.nodes[previous_element[0]]

            print("[BIA - MQ] Elements in comparison (UIDs: %s / %s)" % (current_element[0], previous_element[0]))
            print("[BIA - MQ] Elements : %s -- %s" % (current_element, previous_element))

            # Check if belongs to the structure
            if current_node["tag"] == previous_node["tag"]:

                candidate_for_matched = True

                if current_node["attrs"] != previous_node["attrs"]:
                    print( "[BIA - MQ] Elements' attributes differ!" )

                    candidate_for_matched = False

                    # Local inits
                    current_attr_dictionary: dict = current_node["attrs"]
                    previous_attr_dictionary: dict = previous_node["attrs"]

                    # Checking for behav. attributes
                    current_eh_list = self.event_handler_parser.detectEventHandlers(current_attr_dictionary)
                    previous_eh_list = self.event_handler_parser.detectEventHandlers(previous_attr_dictionary)

                    # Removing event-handlers from original attributes dictionaries
                    if len( current_eh_list ) > 0:
                        for key, value in current_attr_dictionary.copy().items():
                            if key in current_eh_list:
                                current_attr_dictionary.pop(key)

                    if len( previous_eh_list ) > 0:
                        for key, value in previous_attr_dictionary.copy().items():
                            if key in previous_eh_list:
                                previous_attr_dictionary.pop(key)

                    # Comparing both lists
                    EH_comparison = utilities.inspectAttributesForSimilarity(current_eh_list, previous_eh_list)
                    ATTR_comparison = utilities.inspectAttributesForSimilarity(current_attr_dictionary, previous_attr_dictionary)

                    if ATTR_comparison is False:
                        self.presentation_detection.append([current_element, previous_element])

                    if EH_comparison is False:
                        status = "modification of event handlers"
                        self.behaviour_detection.append([current_element, previous_element, status])

                if current_node["SHA_content"] != previous_node["SHA_content"]:
                    print( "[BIA - MQ] Elements' contents differ!" )

                    current_dom = current_node["DOM"]
                    previous_dom = previous_node["DOM"]

                    current_dom_content = [element for element in current_dom if isinstance(element, NavigableString)]
                    previous_dom_content = [element for element in previous_dom if isinstance(element, NavigableString)]

                    percent = fuzz.ratio(current_dom_content, previous_dom_content)

                    if percent >= 40:
                        print( "Text has updated! TSR: %i" % percent )
                        print( "Current content: %s" % current_dom_content )
                        print( "Previous content: %s" % previous_dom_content )
                        status = "Updated"
                    else:
                        print( "Text has modified! TSR: %i" % percent )
                        print( "Current content: %s" % current_dom_content )
                        print( "Previous content: %s" % previous_dom_content )
                        status = "Modified"

                    self.content_detection.append([current_element, previous_element, percent, status])
                    candidate_for_matched = False

                # Condition for identical matching, based on the flag
                if candidate_for_matched is True:
                    print("[BIA - MQ] Elements' are identical!")
                else:
                    print("[BIA - MQ] Elements' structure is the same, inner parameters differ!")

                self.matched_queue.append([current_element, previous_element])
                self.current_queue.remove(current_element)
                self.previous_queue.remove(previous_element)

            else:
                # If tags differ - just removing dependant levels and not processing that element
                print("[BIA - MQ] Tag does not belong to structure!")

                current_element_problematic_children = self.getChildNodes(current_element, 0)
                previous_element_problematic_children = self.getChildNodes(previous_element, 1)

                # Changing the element's format
                current_element = [0, current_element]
                previous_element = [0, previous_element]

                # Force input
                current_element_problematic_children.insert(0, current_element)
                previous_element_problematic_children.insert(0, previous_element)

                # Decomposition
                self.decomposeElementQueue(current_element_problematic_children, 0)
                self.decomposeElementQueue(previous_element_problematic_children, 1)

                # Removing dependant levels
                self.removeDependantLevels(current_element[0], current_subbranch_levels, 0)
                self.removeDependantLevels(previous_element[0], previous_subbranch_levels, 1)
        else:
            print("[BIA - MQ] Elements in the inspection belong to different type of queues (%s -- %s)! Skipping..." % (aloc_current_element[0], aloc_previous_element[0]))

    # Function that inspect two branches (main function for analysis)
    def inspectBranches(self, current_root: list, previous_root: list, current_branches: list, previous_branches: list):

        # Used to indicate the end of iteration on the level and for exceptions
        current_reached_end = False
        previous_reached_end = False

        # Iterators for subbranches
        current_level_iter = iter(current_branches)
        previous_level_iter = iter(previous_branches)

        level_flag = True

        # Start of subbranch iteration
        while True:

            # For extracting element/node in subbranches
            position = 0

            # Exception block for levels
            try:
                selected_current_level = next(current_level_iter)
            except StopIteration:
                break

            try:
                selected_previous_level = next(previous_level_iter)
            except StopIteration:
                break

            # Element iterator on certain level of subbranch
            while level_flag:

                # Exception block for element on the level
                try:
                    current_level_element = selected_current_level[position]
                except IndexError:
                    # Indicator that element on certain level cannot be extracted
                    current_reached_end = True

                try:
                    previous_level_element = selected_previous_level[position]
                except IndexError:
                    # Indicator that element on certain level cannot be extracted
                    previous_reached_end = True

                # If the element on the position cannot be extracted for some element on certain level -> structural problem
                if current_reached_end is True or previous_reached_end is True:

                    # IF both elements cannot be extracted -> exiting the loop
                    if current_reached_end is True and previous_reached_end is True:
                        position = 0

                        # Resetting the flags
                        current_reached_end = previous_reached_end = False
                        break

                    # If previous element cannot be indexed
                    elif current_reached_end is False and previous_reached_end is True:

                        current_level_length = len(selected_current_level)

                        print("[BIA - SP] Found structural change within level for current elements!")

                        for i in range(position, current_level_length):

                            new_structural_element = selected_current_level[i]

                            # Regular element
                            if new_structural_element[0] == 0:

                                # Extracting problematic element
                                current_element = new_structural_element[1]

                                # Extracting children and append root of branch
                                current_element_problematic_children = self.getChildNodes(current_element, 0)
                                current_element_problematic_children.insert(0, [0, current_element])

                                print("[BIA - SP] Regular::Current element children (%s): %s" % (len(current_element_problematic_children), current_element_problematic_children))

                                # Removing false-positives if any
                                self.decomposeElementQueue(current_element_problematic_children, 0)

                                # RE-init global buffers
                                renewed_current_buffer = self.separateIntoLevels(self.getChildNodes(current_root, 0), 0)
                                current_branches.clear()
                                current_branches = renewed_current_buffer
                                self.removeDependantLevels(current_element[0], current_branches, 0)

                                renewed_previous_buffer = self.separateIntoLevels(self.getChildNodes(previous_root, 1), 1)
                                previous_branches.clear()
                                previous_branches = renewed_previous_buffer

                            # Matched
                            elif new_structural_element[0] == 2:

                                # Extract children of problematic element
                                pair = new_structural_element[1]
                                current_element = pair[0]
                                previous_element = pair[1]

                                # Extracting children and append root of branch
                                current_element_problematic_children = self.getChildNodes(current_element, 0)
                                previous_element_problematic_children = self.getChildNodes(previous_element, 1)
                                current_element_problematic_children.insert(0, [0, current_element])
                                previous_element_problematic_children.insert(0, [0, previous_element])

                                try:
                                    self.matched_queue.remove(pair)
                                except ValueError:
                                    print("[BIA - SP] Matched::Pair is not in matched queue!")

                                print("[BIA - SP] Matched::Current element children (%s): %s" % (len(current_element_problematic_children), current_element_problematic_children))
                                print("[BIA - SP] Matched::Previous element children (%s): %s" % (len(previous_element_problematic_children), previous_element_problematic_children))

                                # Removing false-positives if any
                                self.decomposeElementQueue(current_element_problematic_children, 0)
                                self.decomposeElementQueue(previous_element_problematic_children, 1)

                                # RE-init global buffers
                                renewed_current_buffer = self.separateIntoLevels(self.getChildNodes(current_root, 0), 0)
                                current_branches.clear()
                                current_branches = renewed_current_buffer
                                self.removeDependantLevels(current_element[0], current_branches, 0)

                                renewed_previous_buffer = self.separateIntoLevels(self.getChildNodes(previous_root, 1), 1)
                                previous_branches.clear()
                                previous_branches = renewed_previous_buffer

                            else:
                                print("[BIA - SP] Unclassified: ", new_structural_element)

                        # Resetting the flags
                        current_reached_end = previous_reached_end = False
                        position = 0
                        break

                    elif current_reached_end is True and previous_reached_end is False:

                        previous_level_length = len(selected_previous_level)

                        print("[BIA - SP] Found structural change within level for previous elements!")

                        for i in range(position, previous_level_length):

                            new_structural_element = selected_previous_level[i]

                            # Regular element
                            if new_structural_element[0] == 0:

                                # Extract children of problematic element
                                previous_element = new_structural_element[1]

                                # Extracting children and append root of branch
                                previous_element_problematic_children = self.getChildNodes(previous_element, 1)
                                previous_element_problematic_children.insert(0, [0, previous_element])

                                print("[BIA - SP] Regular::Previous element children (%s): %s" % (len(previous_element_problematic_children), previous_element_problematic_children))

                                # Removing false-positives if any
                                self.decomposeElementQueue(previous_element_problematic_children, 1)

                                # RE-init global buffers
                                renewed_previous_buffer = self.separateIntoLevels(self.getChildNodes(previous_root, 1), 1)
                                previous_branches.clear()
                                previous_branches = renewed_previous_buffer
                                self.removeDependantLevels(previous_element[0], previous_branches, 1)

                                renewed_current_buffer = self.separateIntoLevels(self.getChildNodes(current_root, 0), 0)
                                current_branches.clear()
                                current_branches = renewed_current_buffer

                            # Matched
                            elif new_structural_element[0] == 2:

                                # Extract children of problematic element
                                pair = new_structural_element[1]
                                current_element = pair[0]
                                previous_element = pair[1]

                                # Extracting children and append root of branch
                                current_element_problematic_children = self.getChildNodes(current_element, 0)
                                previous_element_problematic_children = self.getChildNodes(previous_element, 1)
                                current_element_problematic_children.insert(0, [0, current_element])
                                previous_element_problematic_children.insert(0, [0, previous_element])

                                print("[BIA - SP] Matched::Current element children (%s): %s" % (len(current_element_problematic_children), current_element_problematic_children))
                                print("[BIA - SP] Matched::Previous element children (%s): %s" % (len(previous_element_problematic_children), previous_element_problematic_children))

                                try:
                                    self.matched_queue.remove(pair)
                                except ValueError:
                                    print("[BIA - SP] Matched::Current pair is not in matched queue!")

                                # Removing false-positives if any
                                self.decomposeElementQueue(current_element_problematic_children, 0)
                                self.decomposeElementQueue(previous_element_problematic_children, 1)

                                # RE-init global buffers
                                renewed_current_buffer = self.separateIntoLevels(self.getChildNodes(current_root, 0), 0)
                                current_branches.clear()
                                current_branches = renewed_current_buffer
                                self.removeDependantLevels(current_element[0], current_branches, 0)

                                renewed_previous_buffer = self.separateIntoLevels(self.getChildNodes(previous_root, 1), 1)
                                previous_branches.clear()
                                previous_branches = renewed_previous_buffer

                            else:
                                print("[BIA - SP] Unclassified: ", new_structural_element)

                        # Resetting the flags
                        current_reached_end = previous_reached_end = False
                        position = 0
                        break

                else:
                    print("\n[BIA - MQ] Elements inspection started")
                    print("[BIA - MQ] Elements : %s -- %s" % (current_level_element, previous_level_element))

                    # Regular processing -> If elements on level belong to current/previous queue
                    if current_level_element[0] == 0 and previous_level_element[0] == 0:

                        current_ext_element = current_level_element[1]
                        previous_ext_element = previous_level_element[1]

                        self.inspectElements(current_ext_element, previous_ext_element, current_branches, previous_branches)

                    # Matched elements -> if both elements belong to matched queue
                    elif current_level_element[0] == 2 and previous_level_element[0] == 2:

                        # If the pairs are identical
                        if current_level_element[1] == previous_level_element[1]:

                            print("[BIA - MQ] Pairs are identical!")

                        else:
                            # Decomposition of matched pairs -> check for moved objects
                            print("\n[BIA - MQ] Paired pairs have some differences")

                            print("[BIA - MQ] Matched elements: %s -- %s" % (current_level_element[1], previous_level_element[1]))

                            # TODO: new blocks added
                            # Extract element from accord. pairs
                            current_extracted_element = current_level_element[1][0]
                            previous_extracted_element = previous_level_element[1][1]

                            # Extracting children and append root of branch
                            current_child_check = self.getChildNodes(current_extracted_element, 0)
                            previous_child_check = self.getChildNodes(previous_extracted_element, 1)
                            current_child_check.insert(0, current_level_element)
                            previous_child_check.insert(0, previous_level_element)

                            # for each in current_child_check:
                            #     print(each)
                            #
                            # print("[BIA - possibleMove] Previous children")
                            #
                            # for each in previous_child_check:
                            #     print(each)

                            # Decompose
                            self.decomposeElementQueue(current_child_check, 0)
                            self.decomposeElementQueue(previous_child_check, 1)

                            # Form branches
                            current_new_branches = self.getChildNodes(current_extracted_element, 0)
                            previous_new_branches = self.getChildNodes(previous_extracted_element, 1)

                            current_new_branches.insert(0, [0, current_extracted_element])
                            previous_new_branches.insert(0, [0, previous_extracted_element])

                            current_new_branches = self.separateIntoLevels(current_new_branches, 0)
                            previous_new_branches = self.separateIntoLevels(previous_new_branches, 1)

                            self.inspectBranches(current_extracted_element, previous_extracted_element, current_new_branches, previous_new_branches)

                            self.removeDependantLevels(current_level_element[1][0][0], current_branches, 0)
                            self.removeDependantLevels(previous_level_element[1][1][0], previous_branches, 1)

                    # Exceptional elements
                    else:
                        # If current element belongs to regular queue and previous element to matched
                        if current_level_element[0] == 0 and previous_level_element[0] == 2:

                            print("[BIA - MQ] Exception processing (regular - matched)")

                            # Extracting the data for analysis
                            current_problematic_element = current_level_element[1]

                            matched_pair = previous_level_element[1]
                            current_paired_problematic_element = matched_pair[0]
                            previous_paired_problematic_element = matched_pair[1]

                            # Re-appending elements to regular queues
                            if current_paired_problematic_element not in self.current_queue:
                                self.current_queue.append(current_paired_problematic_element)

                            if previous_paired_problematic_element not in self.previous_queue:
                                self.previous_queue.append(previous_paired_problematic_element)

                            # Removing matched pair
                            if matched_pair in self.matched_queue:
                                self.matched_queue.remove(matched_pair)

                            # Extracting all related elements
                            current_decomp_children = self.getChildNodes(current_problematic_element, 0)
                            current_matched_decomp_children = self.getChildNodes(current_paired_problematic_element, 0)
                            previous_matched_decomp_children = self.getChildNodes(previous_paired_problematic_element, 1)

                            current_decomp_children.insert(0, [0, current_problematic_element])
                            current_matched_decomp_children.insert(0, [0, current_paired_problematic_element])
                            previous_matched_decomp_children.insert(0, [0, previous_paired_problematic_element])

                            # for c_level in current_decomp_children:
                            #     print(c_level)
                            #
                            # print("------------------------------")
                            #
                            # for p_level in previous_matched_decomp_children:
                            #     print(p_level)

                            # Decomposition
                            self.decomposeElementQueue(current_decomp_children, 0)
                            self.decomposeElementQueue(current_matched_decomp_children, 0)
                            self.decomposeElementQueue(previous_matched_decomp_children, 1)

                            # print("[BIA - Decompose] Branches (regular - matched) are set to additional re-classification!")

                            # Re-init children & branch
                            current_decomp_children.clear()
                            previous_matched_decomp_children.clear()

                            current_decomp_children = self.getChildNodes(current_problematic_element, 0)
                            current_decomp_children.insert(0, [0, current_problematic_element])
                            current_decomp_children = self.separateIntoLevels(current_decomp_children, 0)

                            previous_matched_decomp_children = self.getChildNodes(previous_paired_problematic_element, 1)
                            previous_matched_decomp_children.insert(0, [0, previous_paired_problematic_element])
                            previous_matched_decomp_children = self.separateIntoLevels(previous_matched_decomp_children, 1)

                            print("\n[BIA - MQ] Start of branch inspection (regular - matched)")

                            self.inspectBranches(current_problematic_element, previous_paired_problematic_element, current_decomp_children, previous_matched_decomp_children)

                            self.removeDependantLevels(current_level_element[1][0], current_branches, 0)
                            self.removeDependantLevels(previous_level_element[1][1][0], previous_branches, 1)

                            # current_branches.clear()
                            # previous_branches.clear()
                            #
                            # current_branches = self.separateIntoLevels(self.getChildNodes(current_problematic_element, 0), 0)
                            # self.removeDependantLevels(current_problematic_element[0], current_branches, 0)
                            #
                            # previous_branches = self.separateIntoLevels(self.getChildNodes(previous_paired_problematic_element, 1), 1)
                            # self.removeDependantLevels(previous_paired_problematic_element[0], previous_branches, 1)

                        # Vice versa
                        elif current_level_element[0] == 2 and previous_level_element[0] == 0:
                            print("[BIA - MQ] Exception processing (matched - regular)")

                            # Extracting elements
                            current_problematic_pair = current_level_element[1]
                            current_paired_problematic_element = current_problematic_pair[0]
                            previous_paired_problematic_element = current_problematic_pair[1]

                            previous_problematic_element = previous_level_element[1]

                            # Re-appending elements to regular queues
                            if current_paired_problematic_element not in self.current_queue:
                                self.current_queue.append(current_paired_problematic_element)

                            if previous_paired_problematic_element not in self.previous_queue:
                                self.previous_queue.append(previous_paired_problematic_element)

                            if current_problematic_pair in self.matched_queue:
                                self.matched_queue.remove(current_problematic_pair)

                            # Extracting all related elements
                            previous_decomp_children = self.getChildNodes(previous_problematic_element, 1)
                            current_matched_decomp_children = self.getChildNodes(current_paired_problematic_element, 0)
                            previous_matched_decomp_children = self.getChildNodes(previous_paired_problematic_element, 1)

                            # Appending problematic element (start elements) to the queues
                            previous_decomp_children.insert(0, [0, previous_problematic_element])
                            current_matched_decomp_children.insert(0, [0, current_paired_problematic_element])
                            previous_matched_decomp_children.insert(0, [0, previous_paired_problematic_element])

                            # for c_level in current_matched_decomp_children:
                            #     print(c_level)
                            #
                            # print("------------------------------")
                            #
                            # for p_level in previous_decomp_children:
                            #     print(p_level)

                            # Decomposition
                            self.decomposeElementQueue(previous_decomp_children, 1)
                            self.decomposeElementQueue(current_matched_decomp_children, 0)
                            self.decomposeElementQueue(previous_matched_decomp_children, 1)

                            # Re-init children & branch
                            current_matched_decomp_children.clear()
                            previous_decomp_children.clear()

                            current_matched_decomp_children = self.getChildNodes(current_paired_problematic_element, 0)
                            current_matched_decomp_children.insert(0, [0, current_paired_problematic_element])
                            current_matched_decomp_children = self.separateIntoLevels(current_matched_decomp_children, 0)

                            previous_decomp_children = self.getChildNodes(previous_problematic_element, 1)
                            previous_decomp_children.insert(0, [0, previous_problematic_element])
                            previous_decomp_children = self.separateIntoLevels(previous_decomp_children, 1)

                            print("\n[BIA - MQ] Start of branch inspection (matched - regular)")

                            current_branches.clear()
                            previous_branches.clear()

                            self.inspectBranches(current_paired_problematic_element, previous_problematic_element, current_matched_decomp_children, previous_decomp_children)

                            self.removeDependantLevels(current_level_element[1][0][0], current_branches, 0)
                            self.removeDependantLevels(previous_level_element[1][0], previous_branches, 1)

                            # current_branches = self.separateIntoLevels(self.getChildNodes(current_paired_problematic_element, 0), 0)
                            # self.removeDependantLevels(current_paired_problematic_element[0], current_branches, 0)
                            #
                            # previous_branches = self.separateIntoLevels(self.getChildNodes(previous_problematic_element, 1), 1)
                            # self.removeDependantLevels(previous_problematic_element[0], previous_branches, 1)

                # Increasing the pos. value
                position += 1

    # BIA algorithm
    def inspectMatchedQueue(self):

        # Inits
        matched_queue_iter = iter(self.matched_queue)

        while True:
            try:
                # Extracting pair
                matched_pair = next(matched_queue_iter)

                current_element: list = matched_pair[0]
                previous_element: list = matched_pair[1]

                print("[BIA - MQ] Matched elements in processing, UIDs (%s -- %s)" % (current_element[0], previous_element[0]))

                # Extracting children and cluster them into subbranches
                current_subbranch_levels = self.separateIntoLevels(self.getChildNodes(current_element, 0), 0)
                previous_subbranch_levels = self.separateIntoLevels(self.getChildNodes(previous_element, 1), 1)

                print("[BIA - MQ] Amount of levels (%s -- %s)" % (len(current_subbranch_levels), len(previous_subbranch_levels)))

                self.inspectBranches(current_element, previous_element, current_subbranch_levels, previous_subbranch_levels)

            except StopIteration:
                print("[BIA - MQ] Reached the end of matched queue!")
                break

    # Delete elements from other queues if it belongs to structural changes
    def removeStructuralElementsFromQueues(self):

        counter = 0

        # If queue contains elements
        if len(self.structure_detection) > 0:

            structure_queue_iter = iter(self.structure_detection)

            while True:

                try:
                    structure_element = next(structure_queue_iter)
                except StopIteration:
                    break

                # Extracting necessary data for removal
                first_element: list = None
                second_element: list = None

                if structure_element[0] is not None and isinstance(structure_element[0], list) is True:

                    first_element = structure_element[0]

                    # Cleaning presentation queue
                    presentation_queue_iter = iter(self.presentation_detection)

                    while True:

                        try:
                            presentation_element = next(presentation_queue_iter)
                        except StopIteration:
                            break

                        if first_element == presentation_element[0]:
                            self.presentation_detection.remove(presentation_element)
                            counter += 1

                    # Cleaning content queue
                    content_queue_iter = iter(self.content_detection)

                    while True:

                        try:
                            content_element = next(content_queue_iter)
                        except StopIteration:
                            break

                        if first_element == content_element[0]:
                            self.content_detection.remove(content_element)
                            counter += 1

                    # Cleaning matched queue
                    # TODO: check this --> new block
                    # matched_queue_iter = iter(self.matched_queue)
                    #
                    # while True:
                    #
                    #     try:
                    #         matched_pair = next(matched_queue_iter)
                    #     except StopIteration:
                    #         break
                    #
                    #     if first_element == matched_pair[0]:
                    #         print("[BIA - removeStructural] Found the pair!")
                    #         self.matched_queue.remove(matched_pair)
                    #         self.current_queue.append(matched_pair[0])
                    #         self.previous_queue.append(matched_pair[1])
                    #         counter += 1

                if structure_element[1] is not None and isinstance(structure_element[1], list) is True:

                    second_element = structure_element[1]

                    # Cleaning presentation queue
                    presentation_queue_iter = iter(self.presentation_detection)

                    while True:

                        try:
                            presentation_element = next(presentation_queue_iter)
                        except StopIteration:
                            break

                        if second_element == presentation_element[1]:
                            self.presentation_detection.remove(presentation_element)
                            counter += 1

                    # Cleaning content queue
                    content_queue_iter = iter(self.content_detection)

                    while True:

                        try:
                            content_element = next(content_queue_iter)
                        except StopIteration:
                            break

                        if second_element == content_element[1]:
                            self.content_detection.remove(content_element)
                            counter += 1

                    # Cleaning matched queue
                    # TODO: check this --> new block
                    # matched_queue_iter = iter(self.matched_queue)
                    #
                    # while True:
                    #
                    #     try:
                    #         matched_pair = next(matched_queue_iter)
                    #     except StopIteration:
                    #         break
                    #
                    #     if second_element == matched_pair[1]:
                    #         print("[BIA - removeStructural] Found the pair!")
                    #         self.matched_queue.remove(matched_pair)
                    #         self.current_queue.append(matched_pair[0])
                    #         self.previous_queue.append(matched_pair[1])
                    #         counter += 1

        print("\n[BIA - StructuralCleanUP] Total elements removed: ", counter)

    # Function that compares children signatures for move-operation
    def checkChildrenForSimilarity(self, current_children, previous_children):
        # Local inits
        current_buffer = current_children
        previous_buffer = previous_children

        # If children amount differs and not equal to zero
        if len(current_buffer) != len(previous_buffer):
            return False

        if len(current_buffer) == 0 and len(previous_buffer) == 0:
            return False

        general_length = len(current_buffer)

        # Looping through children
        for i in range(0, general_length):

            current_child = current_buffer[i]
            previous_child = previous_buffer[i]

            # Checking if both children belong to regular/unprocessed queue
            # Reason: to ensure that previous algorithms did a good/correct job of filtering
            if current_child[0] == 0 and previous_child[0] == 0:

                # Extracting data
                current_element = current_child[1]
                previous_element = previous_child[1]

                current_node = current_element[3].nodes[current_element[0]]
                previous_node = previous_element[3].nodes[previous_element[0]]

                # Checking signature
                if current_node["SHA_DOM"] != previous_node["SHA_DOM"]:
                    return False
                else:
                    continue

            elif current_child[0] == 2 and previous_child[0] == 2:

                # Extracting data
                current_element = current_child[1][0]
                previous_element = previous_child[1][1]

                current_node = current_element[3].nodes[current_element[0]]
                previous_node = previous_element[3].nodes[previous_element[0]]

                # Checking signature
                if current_node["SHA_DOM"] != previous_node["SHA_DOM"]:
                    return False
                else:
                    continue

            # If queue indicators differ
            else:
                return False

        return True

    # Function that allocates similar objects from leftovers to find nodes that have been moved in hierarchy
    # To detect move operation, we must find the similar object which parent has been changed
    def detectMovesOperation(self):

        if len(self.current_queue) > 0 and len(self.current_queue) > 0:

            print("\n[BIA - MoveOps] Moved element inspection started! Queue length (%s -- %s)" % (len(self.current_queue), len(self.previous_queue)))

            current_queue_iter = iter(self.current_queue)

            while True:

                try:
                    current_element = next(current_queue_iter)
                except StopIteration:
                    print("[BIA - MoveOps] One of the queues reached is exhausted! No need to look for move operation...")
                    break

                # Extracting data for analysis
                current_node = current_element[3].nodes[current_element[0]]
                current_node_DOM = current_node["SHA_DOM"]

                previous_queue_iter = iter(self.previous_queue)

                while True:

                    try:
                        previous_element = next(previous_queue_iter)
                    except StopIteration:
                        break

                    # Extracting data for analysis
                    previous_node = previous_element[3].nodes[previous_element[0]]
                    previous_node_DOM = previous_node["SHA_DOM"]

                    # Checking parent/root element signatures
                    if current_node_DOM == previous_node_DOM:

                        print("\n[BIA - MoveOps] Current/previous element: %s -- %s" % (current_element, previous_element))

                        current_parent_uid = current_element[1]
                        previous_parent_uid = previous_element[1]

                        current_parent_element = self.extractElementFromQueue(current_parent_uid, 0)
                        previous_parent_element = self.extractElementFromQueue(previous_parent_uid, 1)

                        print("[BIA - MoveOps] Parent of allocated current element: ", current_parent_element)
                        print("[BIA - MoveOps] Parent of allocated previous element: ", previous_parent_element)

                        # previous_allocation_element = previous_parent_element[1][1]
                        # current_allocation_element = current_parent_element[1][0]

                        # Decompose and re-init
                        current_moved_children = self.getChildNodes(current_element, 0)
                        previous_moved_children = self.getChildNodes(previous_element, 1)

                        print("[BIA - MoveOps] Current child elements (before re-classifying) : %s" % current_moved_children)
                        print("[BIA - MoveOps] Previous child elements (before re-classifying) : %s" % previous_moved_children)

                        self.decomposeElementQueue(current_moved_children, 0)
                        self.decomposeElementQueue(previous_moved_children, 1)

                        current_moved_children = self.getChildNodes(current_element, 0)
                        previous_moved_children = self.getChildNodes(previous_element, 1)

                        # If children were allocated
                        if len(current_moved_children) > 0 and len(previous_moved_children) > 0:

                            print("[BIA - MoveOps] Elements contain children...")
                            print("[BIA - MoveOps] Current moved children (%s): %s" % (len(current_moved_children), current_moved_children))
                            print("[BIA - MoveOps] Previous moved children (%s): %s" % (len(previous_moved_children), previous_moved_children))

                            children_match = self.checkChildrenForSimilarity(current_moved_children, previous_moved_children)

                            print("[BIA - MoveOps] Children match: ", children_match)

                            # If children are identical
                            if children_match is True:

                                status = "moved"

                                # Removing current/previous branch roots + appending new structural detection
                                self.current_queue.remove(current_element)
                                self.previous_queue.remove(previous_element)

                                self.structure_detection.append([current_element, previous_element, status])

                                general_length = len(current_moved_children)

                                for i in range(0, general_length):
                                    # Extracting children
                                    # Not checking indicitors -> since it was already checked by function before
                                    current_extracted_element = current_moved_children[i][1]
                                    previous_extraced_element = previous_moved_children[i][1]

                                    # Cleaning the queues and registring children information
                                    if current_extracted_element in self.current_queue:
                                        self.current_queue.remove(current_extracted_element)

                                    if previous_extraced_element in self.previous_queue:
                                        self.previous_queue.remove(previous_extraced_element)

                                    self.structure_detection.append([current_extracted_element, previous_extraced_element, status])

                                break
                            # If not - break the iter
                            else:
                                break

                        # If children were not allocated
                        elif len(current_moved_children) == 0 and len(previous_moved_children) == 0:

                            print("[BIA - MoveOps] Elements have no children...")
                            print("[BIA - MoveOps] Processing similar single elements...")

                            # Cleaning the queues and register children information
                            self.current_queue.remove(current_element)
                            self.previous_queue.remove(previous_element)

                            status = "moved"
                            self.structure_detection.append([current_element, previous_element, status])

                            break

    # Function that classifies leftovers
    def classifyLeftovers(self):
        # Detect move operations
        self.detectMovesOperation()

        # Leftovers from current queue are classified as new/inserted objects
        if len(self.current_queue) > 0:

            for current_element in self.current_queue:
                status = "inserted"
                self.structure_detection.append([current_element, None, status])

            self.current_queue.clear()

        # Leftovers from previous queue are classified as removed/deleted objects
        if len(self.previous_queue) > 0:

            for previous_element in self.previous_queue:
                status = "removed"
                self.structure_detection.append([None, previous_element, status])

            self.previous_queue.clear()

    # Main call
    def inspect(self):
        # Main function
        # Sorting matched array, based on current nodes weight
        self.matched_queue.sort(key=lambda x: x[0][2], reverse=True)
        self.current_queue.sort(key=lambda x: x[2], reverse=True)
        self.previous_queue.sort(key=lambda x: x[2], reverse=True)

        self.inspectMatchedQueue()
        self.classifyLeftovers()

        # Cleaning up the queues
        self.removeStructuralElementsFromQueues()
