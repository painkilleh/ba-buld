# Phase #2 - 2
# Class that is responsible for queue formulation

import networkx as nx


class PriorityQueue:
    # Inits
    def __init__(self, current_tree, previous_tree):
        self.curr_tree: nx.DiGraph = current_tree
        self.prev_tree: nx.DiGraph = previous_tree

    # Main call
    def organize(self):
        def tree2queue(graph):
            queue = []
            for uid, relation in graph.adjacency():

                tmp = [uid]

                if len(relation) != 0:
                    for each in relation:
                        tmp.append(each)

                sub_branch = graph.subgraph(tmp).copy()
                weight = graph.nodes[uid]["weight"]
                parent = graph.nodes[uid]["parent_uid"]
                queue.append([uid, parent, weight, sub_branch])

            # Sorting queue, based on DOM-branches and its child weight
            queue.sort(key=lambda x: x[2], reverse=True)
            return queue

        # Output as python list which contains node's number; node's sum of all children weight; graph that represents it
        curr_queue = tree2queue(self.curr_tree)
        prev_queue = tree2queue(self.prev_tree)
        return [curr_queue, prev_queue]
