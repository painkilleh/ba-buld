# Phase 3 - Post-processing
# Class that is responsible for sequential execution of post-processing sub-classes

import delta_generation as DG


class PostProcessing:

    def __init__(self, queues, current_path, previous_path, total_current, total_previous):
        self.composite_queue = queues
        self.current_path = current_path
        self.previous_path = previous_path
        self.total_current = total_current
        self.total_previous = total_previous

    def execute(self):
        # Generation of report file
        DG.DeltaGen(self.composite_queue, self.current_path, self.previous_path, self.total_current, self.total_previous).generateReport()