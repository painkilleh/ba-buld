# Main execution of Diff algorithm

import os
import pre_processing as preproc
import processing as proc
import post_processing as postproc
import time


def execute():
    # Timing (General)
    general_exec_start = time.perf_counter()

    # Files for tests
    curr_file = "test1/lnu.se.html"
    prev_file = "test2/lnu.se.html"

    # curr_file = "webpages/golf.com/new/GOLF.com_ Golf News, Golf Equipment, Instruction, Courses, Travel.html"
    # prev_file = "webpages/golf.com/old/GOLF.com_ Golf News, Golf Equipment, Instruction, Courses, Travel.html"

    if os.path.isfile(curr_file) and os.path.isfile(prev_file):

        # Timing (Pre)
        preproc_exec_start = time.perf_counter()

        # Phase #1 - Pre-processing
        pre_processing_exit_data = preproc.PreProcessing(curr_file, prev_file).execute()

        preproc_exec_delta = time.perf_counter() - preproc_exec_start

        # Data for post processing -> total amount of discovered elements in web application source file
        current_element_count = len(pre_processing_exit_data[0])
        previous_element_count = len(pre_processing_exit_data[1])

        # Timing (Proc)
        proc_exec_start = time.perf_counter()

        # Phase #2 - Processing
        processing_exit_data = proc.Processing(pre_processing_exit_data).execute()

        proc_exec_delta = time.perf_counter() - proc_exec_start

        # Timing (Post)
        postproc_exec_start = time.perf_counter()

        # Phase #3 - Post-processing
        post_processing_exit_data = postproc.PostProcessing(processing_exit_data, curr_file, prev_file, current_element_count, previous_element_count).execute()

        postproc_exec_delta = time.perf_counter() - postproc_exec_start
        general_exec_delta = time.perf_counter() - general_exec_start

        print("----------------------------")
        print("Execution time")
        print("Pre-processing: %s" % preproc_exec_delta)
        print("Processing: %s" % proc_exec_delta)
        print("Post-processing: %s" % postproc_exec_delta)
        print("----------------------------")
        print("Overall: %s" % general_exec_delta)

    else:
        print("[DIFF-MAIN] Problems with file allocation! Could not open the file...")


execute()
