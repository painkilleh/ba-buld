# Phase #2 -
# WDA stands for Whole DOM Analysis
# Expl: if signatures of two whole DOM objects are the same and their parent labels are identical, it is possible to state
# that both objects are identical and do not need be scanned or processed by BULD following detection rules.

import utils as utilities
import networkx as nx


class WDA:
    # Inits for WDA
    def __init__(self, current_queue, previous_queue, matched_queue):
        self.current_queue: list = current_queue
        self.previous_queue: list = previous_queue
        self.matched_queue: list = matched_queue
        self.wda_buffer: list = []

    # Performs whole DOM analysis
    def DOM_analysis(self, current_element, previous_element):
        # Extracting data for comparison
        current_node = utilities.extractdatafromgraph( current_element[0], current_element[3] )
        previous_node = utilities.extractdatafromgraph( previous_element[0], previous_element[3] )

        # print("[WDA Analysis] Previous queue in comparison [UID: %s | Weight: %s | Label: %s + %s | Signature: %s]" % (previous_element[0], previous_element[2], previous_node["tag"], previous_node["attrs"], previous_node["SHA_DOM"]))

        # Signature check + weight delimiter
        if current_node["SHA_DOM"] == previous_node["SHA_DOM"] and previous_element[2] <= current_element[2]:
            # print("[WDA Analysis] Matching node found from previous queue [UID: %s | Weight: %s | Label: %s + %s]" % (previous_element[0], previous_element[2], previous_node["tag"], previous_node["attrs"]))
            # print( "[WDA Analysis] Matched signatures: %s --- %s" % (current_node["SHA_DOM"], previous_node["SHA_DOM"]) )
            # print("[WDA CRITICAL] Previous element: ", previous_element)
            self.wda_buffer.append([current_element, previous_element])

    # Function for element extraction from queue - current/previous/matched
    def extractElementFromQueue(self, element_uid, mode):

        if isinstance( element_uid, int ):
            if mode == 0:

                # Checking current queue
                for element in self.current_queue:
                    if element_uid == element[0]:
                        return [0, element]

                # Checking matched queue
                for pair in self.matched_queue:
                    selected_element_from_pair = pair[mode]

                    if element_uid == selected_element_from_pair[0]:
                        return [2, pair]

                # If could not find any queues
                return False

            elif mode == 1:

                # Checking current queue
                for element in self.previous_queue:
                    if element_uid == element[0]:
                        return [0, element]

                # Checking matched queue
                for pair in self.matched_queue:

                    selected_element_from_pair = pair[mode]

                    if element_uid == selected_element_from_pair[0]:
                        return [2, pair]

                # If could not find any queues
                return False

            else:
                print( "[WDA - EE] Wrong mode selection!" )
        else:
            print( "[WDA - EE] ID is not int-type!" )

    # Recursively add sub-branch nodes if required
    # Function that extracts children for selected element
    def getChildNodes(self, element, mode):

        # Local variables for storing data
        temp_graph: nx.DiGraph = element[3]
        local_storage: list = []
        local_iter = iter(local_storage)
        skip_flag = False

        # Document-root exception
        if element[0] == 0 and element[1] is None:
            skip_flag = True

        # Extracting data from first element (1st - level -> direct successors)
        if temp_graph.number_of_nodes() > 1 and skip_flag is False:
            for node_id in temp_graph.successors( element[0] ):
                if mode == 0:

                    tmp_node = self.extractElementFromQueue( node_id, 0 )

                    if isinstance( tmp_node, list ):
                        local_storage.append( tmp_node )
                    elif isinstance( tmp_node, bool ) and tmp_node is False:
                        # print("[GCN] Something went wrong!")
                        # print( "1st DEBUG: ", tmp_node)
                        break

                elif mode == 1:

                    tmp_node = self.extractElementFromQueue( node_id, 1 )

                    if isinstance( tmp_node, list ):
                        local_storage.append( tmp_node )
                    elif isinstance( tmp_node, bool ) and tmp_node is False:
                        # print("[GCN] Something went wrong!")
                        # print( "1st DEBUG: ", tmp_node)
                        break

                else:
                    print( "[GCN] Wrong mode!" )
                    break

        # Recursion
        while True and skip_flag is False:
            try:
                internal_element = next( local_iter )

                # If matched child
                if internal_element[0] == 2:
                    if mode == 0:
                        element = internal_element[1][mode]
                        temp_graph = internal_element[1][mode][3]

                    elif mode == 1:
                        element = internal_element[1][mode]
                        temp_graph = internal_element[1][mode][3]

                # If regular child
                else:
                    element = internal_element[1]
                    temp_graph = internal_element[1][3]

                if temp_graph.number_of_nodes() > 1:
                    for node_id in temp_graph.successors( element[0] ):

                        if mode == 0:

                            tmp_node = self.extractElementFromQueue( node_id, 0 )

                            if isinstance( tmp_node, list ):
                                local_storage.append( tmp_node )
                            elif isinstance( tmp_node, bool ) and tmp_node is False:
                                # print("[GCN] Something went wrong!")
                                # print( "Iter DEBUG: ", tmp_node)
                                break

                        elif mode == 1:

                            tmp_node = self.extractElementFromQueue( node_id, 1 )

                            if isinstance( tmp_node, list ):
                                local_storage.append( tmp_node )
                            elif isinstance( tmp_node, bool ) and tmp_node is False:
                                # print("[GCN] Something went wrong!")
                                # print( "Iter DEBUG: ", tmp_node )
                                break

                        else:
                            print( "[GCN] Wrong mode!" )
                            break

            except StopIteration:
                break

        return local_storage

    # Flushing procedure
    def flushChildren(self, current_element):
        print( "[WDA Flush] No element in buffer!" )

        current_children = self.getChildNodes(current_element, 0)

        matched_element_count = 0
        regular_element_count = 0

        for child in current_children:
            # If matched pair
            if child[0] == 2:
                matched_element_count += 1
            # If regular element
            elif child[0] == 0:
                self.current_queue.remove(child[1])
                self.current_queue.append(child[1])
                regular_element_count += 1

        print("[WDA Flush] Element: %s stays in queue and its children re-appended to the end of queue, children (from queue: %s; from matched: %s) \n" % (current_element, regular_element_count, matched_element_count))

    # Check the branch elements for similarity (amount of nodes, sequence and signatures)
    def checkBranchSimilarity(self, current_children_queue, previous_children_queue):

        current_length = len(current_children_queue)
        previous_length = len(previous_children_queue)

        # Amount check
        if current_length != previous_length:
            print( "[WDA - CBS] Amount of nodes in branches differ!" )
            return False

        elif current_length == previous_length:

            general_length = current_length

            # Looping through children
            for i in range( 0, general_length ):

                # Extracting data
                current_selected_element = current_children_queue[i]
                previous_selected_element = previous_children_queue[i]

                # Checking indicators of element
                # 0 - for regular
                # 2 - for matched

                # Comparison -> regular against regular
                if current_selected_element[0] == 0 and previous_selected_element[0] == 0:

                    # Extracting data
                    current_element = current_selected_element[1]
                    previous_element = previous_selected_element[1]

                    current_node = current_element[3].nodes[current_element[0]]
                    previous_node = previous_element[3].nodes[previous_element[0]]

                    if current_node["SHA_DOM"] != previous_node["SHA_DOM"]:
                        print( "[WDA - CBS] Signatures of selected children differ (UIDs: %s -- %s)! Exiting..." % (current_element[0], previous_element[0]))
                        return False

                # Comparison -> matched against matched
                elif current_selected_element[0] == 2 and previous_selected_element[0] == 2:

                    # Extracting data
                    current_element_pair = current_selected_element[1]
                    previous_element_pair = previous_selected_element[1]

                    if current_element_pair != previous_element_pair:
                        print("[WDA - CBS] Matched element pairs differ! Exiting...")
                        print("[WDA - CBS] Pairs to inspect: %s -- %s" % (current_element_pair, previous_element_pair))
                        return False
                # Other combinations -> regular against matched and vice versa -> now, considering wrong...
                else:
                    print( "[WDA - CBS] Exceptional matching of elements!" )
                    print( "[WDA - CBS] Elements: %s -- %s" % (current_selected_element, previous_selected_element) )
                    return False

            return True

    # Main function
    def analyse(self):
        # Start
        print( "\n[WDA] Start of WDA" )

        # for current_element in self.current_queue[:]:
        #     # Extracting items for comparison
        #     current_node = utilities.extractdatafromgraph( current_element[0], current_element[3] )
        #     print( "\n[WDA] Current node in process [UID: %s | Weight: %s | Label: %s + %s | Signature: %s]" % (
        #     current_element[0], current_element[2], current_node["tag"], current_node["attrs"], current_node["SHA_DOM"]) )
        #
        #     # Clearing the buffer
        #     self.wda_buffer.clear()
        #
        #     for previous_element in self.previous_queue[:]:
        #         # Extracting items for comparison
        #         # previous_node = utilities.extractdatafromgraph(previous_element[0], previous_element[3])
        #         self.DOM_analysis( current_element, previous_element )

        current_queue_iter = iter(self.current_queue)

        while True:

            try:
                current_element = next(current_queue_iter)
            except StopIteration:
                print("End of queue!")
                break

            current_node = current_element[3].nodes[current_element[0]]

            print("\n[WDA] Current node in process [UID: %s | Weight: %s | Label: %s + %s | Signature: %s]" % (current_element[0], current_element[2], current_node["tag"], current_node["attrs"], current_node["SHA_DOM"]))

            previous_queue_iter = iter(self.previous_queue)

            self.wda_buffer.clear()

            while True:

                try:
                    previous_element = next(previous_queue_iter)
                except StopIteration:
                    break

                self.DOM_analysis(current_element, previous_element)

            # ---------------------------------------------------------------------------------------------------
            # Buffer processing
            # ---------------------------------------------------------------------------------------------------

            # Storage for children
            current_children = []
            previous_children = []

            # If zero elements in buffer -> flushing the queue (meaning all child elements must be added to the end of queue)
            if len(self.wda_buffer) == 0:

                self.flushChildren(current_element)

            # If it is equal to one, check element + its children for similarity -> if identical - append elements accord. ; if not -> flushing procedure execution
            elif len(self.wda_buffer) == 1:

                print("[WDA Buffer - Atomic] One element in buffer: \n%s" % self.wda_buffer)

                # Extracting children nodes for element
                buffer_element = self.wda_buffer[0]

                buffer_current_element = buffer_element[0]
                buffer_previous_element = buffer_element[1]

                current_children = self.getChildNodes( buffer_current_element, 0 )
                previous_children = self.getChildNodes( buffer_previous_element, 1 )

                # print("[WDA Buffer] Current children: ", current_children)
                # print("[WDA Buffer] Previous children: ", previous_children)

                # Children counts are not equal to zero
                if len(current_children) != 0 and len(previous_children) != 0:

                    # Checking if branches are identical
                    if self.checkBranchSimilarity( current_children, previous_children ) is True:

                        print( "[WDA Buffer - Atomic] Sub-branches are identical. Generating pair for matched queue!" )

                        # print(current_children)
                        # print("--------------")
                        # print(previous_children)

                        general_length = len( current_children )

                        # Iterating through children buffers
                        for i in range( 0, general_length ):

                            current_selected_element = current_children[i]
                            previous_selected_element = previous_children[i]

                            # Checking queue indicators
                            # 0 - regular
                            # 2 - matched
                            # Regular element check
                            if current_selected_element[0] == 0 and previous_selected_element[0] == 0:
                                # Extraction of the data
                                current_extracted_element = current_selected_element[1]
                                previous_extracted_element = previous_selected_element[1]

                                # Appending elements accordingly
                                self.current_queue.remove(current_extracted_element)
                                self.previous_queue.remove(previous_extracted_element)
                                self.matched_queue.append([current_extracted_element, previous_extracted_element])

                            # Matched pairs
                            elif current_selected_element[0] == 2 and previous_selected_element[0] == 2:

                                current_extracted_pair = current_selected_element[1]
                                previous_extracted_pair = previous_selected_element[1]

                                if current_extracted_pair == previous_extracted_pair:
                                    continue
                                else:
                                    print( "[WDA Buffer - Atomic] Pairs are not correctly matched:" )
                                    print( "[WDA Buffer - Atomic] Elements pair: %s -- %s" % (current_extracted_pair, previous_extracted_pair) )
                            # Exception
                            else:
                                print( "[WDA Buffer - Atomic] Exceptional matching of elements!" )
                                print( "[WDA Buffer - Atomic] Elements: %s -- %s" % (current_selected_element, previous_selected_element) )

                        # Performing allocation to branch root element
                        if (buffer_current_element in self.current_queue) and (buffer_previous_element in self.previous_queue):
                            self.current_queue.remove(buffer_current_element)
                            self.previous_queue.remove(buffer_previous_element)
                            self.matched_queue.append([buffer_current_element, buffer_previous_element])
                        else:
                            print("[WDA Buffer - Atomic] Branch root elements were not found in current or previous queues!")
                    else:
                        # If buffer has problems -> flushing
                        print("[WDA Buffer - Atomic] Sub-branch for element differs! Flushing...")
                        self.flushChildren(buffer_current_element)

                # If nodes do not have children, but parent node is identical -> apply actions upon buffered elements
                elif len(current_children) == 0 and len(previous_children) == 0:

                    print("[WDA Buffer - Atomic] Branch root-elements have no children (Singular data)")

                    # Performing allocation to branch root element
                    if (buffer_current_element in self.current_queue) and (buffer_previous_element in self.previous_queue):
                        self.current_queue.remove(buffer_current_element)
                        self.previous_queue.remove(buffer_previous_element)
                        self.matched_queue.append([buffer_current_element, buffer_previous_element])
                    else:
                        print("[WDA Buffer - Atomic] Branch root elements were not found in current or previous queues!")

            elif len(self.wda_buffer) > 1:

                print("[WDA - Multiple branches] %s elements were found in buffer: \n %s \n" % (len(self.wda_buffer), self.wda_buffer))

                # Storage for children arrays
                previous_children_storage = []

                current_children = self.getChildNodes(current_element, 0)

                print("[WDA Buffer - Multiple branches] Current children: ", current_children)

                # Extracting the nodes from relative nodes
                for each in self.wda_buffer:
                    previous_buffer_element = each[1]
                    previous_children = self.getChildNodes(previous_buffer_element, 1)

                    print("[WDA Buffer - Multiple branches] Previous children: ", previous_children)

                    previous_children_storage.append(previous_children)

                # Checking every branch for similarity
                # for pos in range(len(previous_children_storage)):
                #
                #     if self.checkBranchSimilarity(current_children, previous_children_storage[pos]) is True:
                #         continue
                #     else:
                #         print("[WDA Buffer - Multiple branches] Branch (pos in buffer: %s) cannot be matched with current element branch! Removing element..." % pos)
                #         previous_children_storage.remove(previous_children_storage[pos])
                #
                #         #self.wda_buffer.remove(self.wda_buffer.index(pos))
                #         self.wda_buffer.remove(self.wda_buffer[pos])

                # Checking every branch for similarity
                previous_children_iter = iter(previous_children_storage)
                pos = 0

                while True:

                    try:
                        selected_ext_children = next(previous_children_iter)
                    except StopIteration:
                        break

                    if self.checkBranchSimilarity(current_children, selected_ext_children) is True:
                        continue
                    else:
                        print("[WDA Buffer - Multiple branches] Branch cannot be matched with current element branch! Removing element...")

                        previous_children_storage.remove(selected_ext_children)
                        self.wda_buffer.remove(self.wda_buffer[pos])

                    pos += 1

                # Checking branch buffer again
                if len(self.wda_buffer) == 0:

                    self.flushChildren(current_element)

                elif len(self.wda_buffer) == 1:

                    print( "[WDA Buffer - Atomic] One element in buffer: \n%s" % self.wda_buffer)

                    # Extracting children nodes for element
                    buffer_element = self.wda_buffer[0]

                    buffer_current_element = buffer_element[0]
                    buffer_previous_element = buffer_element[1]

                    current_children = self.getChildNodes(buffer_current_element, 0)
                    previous_children = self.getChildNodes(buffer_previous_element, 1)

                    print("[WDA Buffer - Atomic] Current children: ", current_children)
                    print("[WDA Buffer - Atomic] Previous children: ", previous_children)

                    # Children counts are not equal to zero
                    if len(current_children) != 0 and len(previous_children) != 0:

                        # Checking if branches are identical
                        # if self.checkBranchSimilarity(current_children, previous_children) is True:

                        print("[WDA Buffer - Atomic] Sub-branches are identical. Generating pair for matched queue!")

                        # print(current_children)
                        # print("--------------")
                        # print(previous_children)

                        general_length = len(current_children)

                        # Iterating through children buffers
                        for i in range(0, general_length):

                            current_selected_element = current_children[i]
                            previous_selected_element = previous_children[i]

                            # Checking queue indicators
                            # 0 - regular
                            # 2 - matched
                            # Regular element check
                            if current_selected_element[0] == 0 and previous_selected_element[0] == 0:
                                # Extraction of the data
                                current_extracted_element = current_selected_element[1]
                                previous_extracted_element = previous_selected_element[1]

                                # Appending elements accordingly
                                self.current_queue.remove(current_extracted_element)
                                self.previous_queue.remove(previous_extracted_element)
                                self.matched_queue.append([current_extracted_element, previous_extracted_element])

                            # Matched pairs
                            elif current_selected_element[0] == 2 and previous_selected_element[0] == 2:

                                current_extracted_pair = current_selected_element[1]
                                previous_extracted_pair = previous_selected_element[1]

                                if current_extracted_pair == previous_extracted_pair:
                                    continue
                                else:
                                    print("[WDA Buffer - Atomic] Pairs are not correctly matched:")
                                    print("[WDA Buffer - Atomic] Elements pair: %s -- %s" % (current_extracted_pair, previous_extracted_pair))
                            # Exception
                            else:
                                print("[WDA Buffer - Atomic] Exceptional matching of elements!")
                                print("[WDA Buffer - Atomic] Elements: %s -- %s" % (current_selected_element, previous_selected_element))

                        # Performing allocation to branch root element
                        if (buffer_current_element in self.current_queue) and (buffer_previous_element in self.previous_queue):
                            self.current_queue.remove(buffer_current_element)
                            self.previous_queue.remove(buffer_previous_element)
                            self.matched_queue.append([buffer_current_element, buffer_previous_element])
                        else:
                            print("[WDA Buffer - Atomic] Branch root elements were not found in current or previous queues!")
                        # else:
                        #     # If buffer has problems -> flushing
                        #     print("[WDA Buffer - Atomic] Sub-branch for element differs! Flushing...")
                        #     self.flushChildren(buffer_current_element)

                # If buffer contains numerous identical branches -> apply relativity metrics, based on node discovery within document (uID)
                elif len(self.wda_buffer) > 1:

                    min_relative_index = self.wda_buffer.index(min(self.wda_buffer, key=lambda x: x[0]))

                    print("[WDA - Multiple branches] Minimal relative index: %s ; Element: %s" % (min_relative_index, self.wda_buffer[min_relative_index]))

                    # Choosing appr. pair
                    selected_pair = self.wda_buffer[min_relative_index]

                    # Choosing appr. elements
                    selected_current = selected_pair[0]
                    selected_previous = selected_pair[1]

                    # Extract children
                    selected_current_children = self.getChildNodes(selected_current, 0)
                    selected_previous_children = self.getChildNodes(selected_previous, 1)

                    print( "[WDA - Multiple branches] Element (%s --- %s) and its children (%s --- %s) were added to matched array." % (selected_current, selected_previous, len(selected_current_children), len(selected_previous_children)))

                    for i in range(len(selected_current_children)):

                        current_selected_element = selected_current_children[i]
                        previous_selected_element = selected_previous_children[i]

                        # Checking queue indicators
                        # 0 - regular
                        # 2 - matched
                        # Regular element check

                        if current_selected_element[0] == 0 and previous_selected_element[0] == 0:
                            # Extraction of the data
                            current_extracted_element = current_selected_element[1]
                            previous_extracted_element = previous_selected_element[1]

                            # Appending elements accordingly
                            self.current_queue.remove(current_extracted_element)
                            self.previous_queue.remove(previous_extracted_element)
                            self.matched_queue.append([current_extracted_element, previous_extracted_element])

                        # Matched pairs
                        elif current_selected_element[0] == 2 and previous_selected_element[0] == 2:

                            current_extracted_pair = current_selected_element[1]
                            previous_extracted_pair = previous_selected_element[1]

                            if current_extracted_pair == previous_extracted_pair:
                                continue
                            else:
                                print("[WDA - Multiple branches] Pairs are not correctly matched")
                                print("[WDA - Multiple branches] Elements pair: %s -- %s" % (current_extracted_pair, previous_extracted_pair))
                        # Exception
                        else:
                            print("[WDA - Multiple branches] Exceptional matching of elements!")
                            print("[WDA - Multiple branches] Elements: %s -- %s" % (current_selected_element, previous_selected_element))

                    if (selected_current in self.current_queue) and (selected_previous in self.previous_queue):
                        self.matched_queue.append([selected_current, selected_previous])
                        self.current_queue.remove(selected_current)
                        self.previous_queue.remove(selected_previous)
                    # else:
                    #     print("WDA BUFFER (n - no children) removal of the objects: %s -- %s" % (current_results, previous_results))

            else:
                print("[WDA Iteration results] No condition matched for processing!")

            self.wda_buffer.clear()
