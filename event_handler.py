# Separated class for event handler detection
# Reason for separation: usage outside of BPP is required without calling/creating instance of BPP
# EHD - stands for Event Handler Detection

import os


class EHD:

    def __init__(self):
        # Buffer for pre-loaded EH identificator
        self.dict_dir: str = "supportive files"
        self.eh_buffer: list = self.initEHDict()

    # Init dictionaries for EHs
    def initEHDict(self):

        eh_buffer: list = []

        for file in os.listdir(self.dict_dir):
            if file.endswith(".txt"):

                with open(self.dict_dir + "/" + file, "r", encoding="utf8") as dict_file:
                    for line in dict_file:
                        eh_buffer.append(line.rstrip("\n"))

                dict_file.close()

        # print("EH buffer: ", eh_buffer)

        return eh_buffer

    # 3. In an event handler, specified as the value of an HTML attribute such as onclick or onmouseover
    def detectEventHandlers(self, attributes: dict):

        if self.eh_buffer is None:
            self.initEHDict()

        event_index_buffer: dict = {}

        for key, value in attributes.items():
            if key.startswith("on", 0, 2) is True:
                # Slicing off first two symbols of EH -> on<event_handler>
                eh_identifier = key[2:]

                for b_identifier in self.eh_buffer:

                    if b_identifier == eh_identifier:
                        event_index_buffer[key] = value
                        break

        return event_index_buffer
